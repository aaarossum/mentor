import pkg from './package'

export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    script: [],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [
    { src: '@/node_modules/ant-design-vue/dist/antd.css', lang: 'css' },
    '~/css/main.css'
  ],
  workbox: {
    cacheAssets: false, // for /*
    offline: false // for /_nuxt/*
  },

  /*
   ** Plugins to load before mounting the App
   */

  plugins: [
    '~/api/init.js',
    { src: '~plugins/vue-infinite-scroll.js', ssr: false }
  ],

  router: {
    middleware: ['auth'],
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'preference',
        path: '/admin/preference/:studentId',
        component: resolve(__dirname, 'pages/admin/preference.vue')
      })
    }
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy'
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/auth/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/api/auth/user', method: 'get', propertyName: 'user' }
        }
      }
    }
  },

  env: {},
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    loaders: {
      less: { javascriptEnabled: true }
    },
    babel: {
      plugins: [['import', { libraryName: 'ant-design-vue' }, 'ant-design-vue']]
    },
    watch: ['api'],
    extend(config, { isDev, isClient }) {
      config.node = {
        fs: 'empty'
      }
    }
  },
  serverMiddleware: ['~/api']
}
