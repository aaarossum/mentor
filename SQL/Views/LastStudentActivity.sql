CREATE VIEW LastStudentActivity AS
    SELECT DISTINCT ON (A.StudentId) StudentId,
	A.DateCreated,
	T.Descr
    FROM public.Activity A
	INNER JOIN public.ActivityType T ON A.TypeId = T.Id
	ORDER BY A.StudentId, DateCreated DESC;
GRANT SELECT ON LastStudentActivity TO webusers;