CREATE VIEW Priority AS
    SELECT DISTINCT(PriorityId) AS Id
    FROM Preference
	ORDER BY PriorityId;
GRANT SELECT ON Priority TO webusers;