CREATE VIEW RankList AS
SELECT T.StudentId, T.ProfessorId, PriorityId, AverageGrade, OwnChoice,
			ROW_NUMBER () OVER 
				(
				PARTITION BY T.ProfessorId
				ORDER BY T.ProfessorId, OwnChoice DESC, AverageGrade DESC
				) AS RankId,
			CASE WHEN R.StudentId IS NULL THEN False ELSE True END AS IsRejected
		FROM
		(
			
		SELECT S.Id AS StudentId,
			GetAverageGrade(S.Id, P.Id) AS AverageGrade,
			P.Id AS ProfessorId,
			PR.PriorityId,
			CASE WHEN PR.PriorityId IS NULL THEN False ELSE True END AS OwnChoice
		FROM Professor P
		CROSS JOIN Student S
		LEFT JOIN Preference PR ON PR.ProfessorId = P.Id AND PR.StudentId = S.Id
		ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC	
		) T
		LEFT JOIN public.Reject R ON T.ProfessorId = R.ProfessorId AND T.StudentId = R.StudentId
		;
GRANT SELECT ON RankList TO webusers;