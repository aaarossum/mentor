CREATE OR REPLACE FUNCTION GetConfig(
)
  RETURNS TABLE (
	  MaxStudents INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT C.MaxStudents
		  FROM public.Config C
		  LIMIT 1;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetConfig();