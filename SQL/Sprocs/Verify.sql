CREATE OR REPLACE FUNCTION Verify(
	_Email TEXT,
	_Code TEXT
)
  RETURNS TABLE (
	  ErrorMessage TEXT
  )
  AS
  $BODY$
  DECLARE
  _SavedCode TEXT;
  _AccountExists BOOL;
      BEGIN
	  
	  	_SavedCode := Code FROM public.Verification WHERE Email = _Email ORDER BY DateCreated DESC LIMIT 1;
		
		IF _Code <> _SavedCode THEN
			RETURN QUERY
			SELECT 'Kod je netočan ili zastarjeo.';
			
		ELSE
			RETURN QUERY
			SELECT '';
		
		END IF;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM Verify('btopic@tvz.hr', '6527');