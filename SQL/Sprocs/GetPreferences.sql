CREATE OR REPLACE FUNCTION GetPreferences(
	_StudentId INT
)
  RETURNS TABLE (
	  ProfessorId INT,
	  PriorityId INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT P.ProfessorId,
			P.PriorityId
		  FROM public.Preference P
		  WHERE P.StudentId = _StudentId;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetPreferences(1);  