CREATE OR REPLACE FUNCTION GetAccount(
	_Id INT,
	_Pass BOOL DEFAULT FALSE
)
  RETURNS TABLE (
	  Id INT,
	  Username TEXT,
	  TypeId INT,
	  Password TEXT,
	  StudentId INT,
	  ProfessorId INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT A.Id,
			A.Username,
			A.TypeId,
			CASE WHEN (_Pass = TRUE) THEN A.Password ELSE '' END AS Password,
			COALESCE(S.Id,0) AS StudentId,
			COALESCE(P.Id,0) AS ProfessorId
		  FROM public.Account A
		  LEFT JOIN public.Student S ON S.AccountId = A.Id
		  LEFT JOIN public.Professor P ON P.AccountId = A.Id
		  WHERE A.Id = _Id;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetAccount(3, TRUE);