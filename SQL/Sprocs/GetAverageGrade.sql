CREATE OR REPLACE FUNCTION GetAverageGrade(
	_StudentId INT,
    _ProfessorId INT
	)
  RETURNS DECIMAL AS
  $BODY$
  DECLARE _USD DECIMAL;
      BEGIN
		RETURN (
		SELECT COALESCE(AVG(G.Grade),0)
        FROM public.Student S
        INNER JOIN public.Contact C ON S.ContactId = C.Id
        LEFT JOIN public.Grade G ON G.StudentId = S.Id
        WHERE G.ProfessorId = _ProfessorId 
        		AND S.Id = _StudentId );
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT GetAverageGrade(1,1);