CREATE OR REPLACE FUNCTION SaveConfig(
	_MaxStudents INT,
	_AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE _TypeId INT;

      BEGIN

	  _TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  IF _TypeId = 100 THEN
		  UPDATE public.Config SET
		  MaxStudents = _MaxStudents;
	  END IF;
	  
      END  
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT SaveConfig(15,1);