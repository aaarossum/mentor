CREATE OR REPLACE FUNCTION CheckAccount(
	_Email TEXT
)
  RETURNS TABLE (
	  ErrorMessage TEXT
  )
  AS
  $BODY$
  DECLARE
  _ContactExists BOOL;
  _AccountExists BOOL;
      BEGIN
	  
	  	_ContactExists := CASE WHEN EXISTS(SELECT Id FROM public.Contact WHERE Email = _Email LIMIT 1) THEN TRUE ELSE FALSE END;
		_AccountExists := CASE WHEN EXISTS(SELECT Id FROM public.Account WHERE Username = _Email LIMIT 1) THEN TRUE ELSE FALSE END;
		
		IF _ContactExists = FALSE THEN
			RETURN QUERY
			SELECT 'Profesor ili student sa ovom e-mail adresom nije pronađen.';
		
		ELSIF _AccountExists = TRUE THEN
			RETURN QUERY
			SELECT 'Korisnički račun sa ovom e-mail adresom je već registriran.';
		
		ELSE
			
			INSERT INTO
			public.Verification
			(Email, Code)
			SELECT 
			_Email, '0000'; -- random code: CAST(floor(random()*(9999-1000+1))+1000 AS TEXT);
			
			RETURN QUERY
			SELECT '';
		
		END IF;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM CheckAccount('btopic@tvz.hr');
