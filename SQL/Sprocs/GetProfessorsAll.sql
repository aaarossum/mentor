CREATE OR REPLACE FUNCTION GetProfessorsAll(
)
  RETURNS TABLE (
	  Id INT,
	  AccountId INT,
	  IsMentor BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT
  )
  AS
  $BODY$
      BEGIN
	  	  RETURN QUERY
	      SELECT P.Id,
			  P.AccountId,
			  P.IsMentor,
			  P.DateCreated,
			  P.DateUpdated,
			  C.Email,
			  C.FirstName,
			  C.LastName,
			  C.Prefix,
			  C.Suffix,
			  C.JMBAG,
			  C.Phone,
			  C.JobDescr,
			  C.VPN
		  FROM public.Professor P
		  INNER JOIN public.Contact C ON P.ContactId = C.Id
		  ORDER BY C.LastName;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetProfessorsAll();