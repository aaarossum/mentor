CREATE OR REPLACE FUNCTION GenerateStudents(
	_Count INT DEFAULT 5
)
  RETURNS void 
  AS
  $BODY$
  DECLARE
  _JMBAG TEXT;
  _Email TEXT;
  _Name TEXT;
  _Surname TEXT;
  _i INT := 0;
      BEGIN
	  
	  	LOOP
			EXIT WHEN _i = _Count; 
      		_i := _i + 1;
			
			_Name := Descr FROM public.Name 
			OFFSET floor(random() * (SELECT COUNT(*) FROM public.Name)) LIMIT 1;

			_Surname := Descr FROM public.Surname 
			OFFSET floor(random() * (SELECT COUNT(*) FROM public.Surname)) LIMIT 1;

			_Email := LEFT(LOWER(_Name),1) || LOWER(_Surname) || '@tvz.hr';
			
			_Email := REPLACE(_Email, 'ć', 'c');
			_Email := REPLACE(_Email, 'č', 'c');
			_Email := REPLACE(_Email, 'đ', 'd');
			_Email := REPLACE(_Email, 'š', 's');
			_Email := REPLACE(_Email, 'ž', 'z');

			_JMBAG := '0249' || CAST(floor(random()*(999999-100000+1))+100000 AS TEXT);

			WITH NewContact AS(
				INSERT INTO public.Contact (Email, FirstName, LastName, JMBAG)
				SELECT _Email, _Name, _Surname, _JMBAG
				RETURNING Id
			)
			INSERT INTO public.Student (ContactId)
			SELECT Id FROM NewContact;
			
		END LOOP;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

--SELECT * FROM GenerateStudents(100);