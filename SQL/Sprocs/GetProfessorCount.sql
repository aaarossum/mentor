CREATE OR REPLACE FUNCTION GetProfessorCount(
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(P.Id) AS INT)
		  FROM public.Professor P
		  INNER JOIN public.Contact C ON P.ContactId = C.Id;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetProfessorCount();