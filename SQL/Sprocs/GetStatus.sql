CREATE OR REPLACE FUNCTION GetStatus(
	_AccountId INT
)
  RETURNS TABLE (
      ProfessorId INT,
	  PriorityId INT,
	  AverageGrade DECIMAL,
	  OwnChoice BOOL,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
		SELECT R._ProfessorId,
			R._PriorityId, 
			R._AverageGrade, 
			R._OwnChoice, 
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN
		FROM
		public.GetRanks() R
		INNER JOIN public.Professor P ON R._ProfessorId = P.Id
		INNER JOIN public.Contact C ON P.ContactId = C.Id
		INNER JOIN public.Student S ON R._StudentId = S.Id
		INNER JOIN public.Account A ON S.AccountId = A.Id
		WHERE A.Id = _AccountId
		LIMIT 1;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetStatus(3);