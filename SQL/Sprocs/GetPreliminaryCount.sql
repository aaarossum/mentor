CREATE OR REPLACE FUNCTION GetPreliminaryCount(
	_ProfessorId INT
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(R.RankId) AS INT)
		FROM
		public.RankList R
		INNER JOIN public.Student S ON R.StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		WHERE R.ProfessorId = _ProfessorId
		;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetPreliminaryCount(111);