CREATE OR REPLACE FUNCTION GetFinalCount(
	_ProfessorIdx INT
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(*) AS INT)
		FROM
		public.GetRanks() R
		INNER JOIN public.Student S ON R._StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		WHERE R._ProfessorId = _ProfessorIdx
		;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetFinalCount(1);