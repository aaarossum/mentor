CREATE OR REPLACE FUNCTION GetStudents(
    _PageId INT,
	_PageSize INT,
	_ProfessorIdX INT DEFAULT 0
)
  RETURNS TABLE (
	  RowId BIGINT,
	  Id INTEGER,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  AccountId INT,
      Username TEXT,
      TypeId INT,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  ProfessorId INT,
	  ProfessorFirstName TEXT,
	  ProfessorLastName TEXT,
	  ProfessorPrefix TEXT,
	  ProfessorSuffix TEXT,
	  ActivityDescr TEXT,
	  ActivityDate TIMESTAMP,
	  IsRejected BOOL
  )
  AS
  $BODY$
      BEGIN
	  	  RETURN QUERY
		  SELECT * 
		  FROM
		  (
	      SELECT ROW_NUMBER () OVER (
			  ORDER BY S.Id DESC 
		   ) AS _RowId,
		  	S.Id,
		  	S.DateCreated,
			S.DateUpdated,
		  	A.Id AS AccountId,
		  	A.Username,
		  	A.TypeId,
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN,
			R._ProfessorId,
			PC.FirstName AS ProfessorFirstName,
			PC.LastName AS ProfessorLastName,
			PC.Prefix AS ProfessorPrefix,
			PC.Suffix AS ProfessorSuffix,
			LA.Descr AS ActivityDescr,
			LA.DateCreated AS ActivityDate,
			CASE WHEN (RJ.ProfessorId = _ProfessorIdX) THEN TRUE ELSE FALSE END AS IsRejected
		  FROM public.Student S
		  LEFT JOIN public.Account A ON S.AccountId = A.Id
		  INNER JOIN public.Contact C ON S.ContactId = C.Id
		  LEFT JOIN public.GetRanks() R ON R._StudentId = S.Id
		  LEFT JOIN public.Professor P ON P.Id = R._ProfessorId
		  LEFT JOIN public.Contact PC ON PC.Id = P.ContactId
		  LEFT JOIN public.LastStudentActivity LA ON LA.StudentId = S.Id
		  LEFT JOIN public.Reject RJ ON RJ.StudentId = S.Id AND RJ.ProfessorId = _ProfessorIdX
		  LIMIT (_PageSize * _PageId)
		  ) T
		  WHERE _RowId > ((_PageId - 1) * _PageSize);
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetStudents(1, 8, 1);