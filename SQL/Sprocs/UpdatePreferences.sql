CREATE OR REPLACE FUNCTION UpdatePreferences(
	_Preferences TEXT,
	_StudentId INT,
    _AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE
  _StudentAccountId INT;
  _TypeId INT;
      BEGIN
	  	_TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
		_StudentAccountId := A.Id 
		FROM public.Account A
		INNER JOIN public.Student S ON S.AccountId = A.Id
		WHERE A.Id = _AccountId;
	  
	    IF (_TypeId = 100 OR _AccountId = _StudentAccountId) THEN
			DELETE FROM public.Preference
			WHERE StudentId = _StudentId;

			IF (_Preferences <> '' AND _Preferences <> 'null') THEN
				INSERT INTO public.Preference
				(StudentId, ProfessorId, PriorityId)
				SELECT _StudentId,
				ProfessorId,
				ROW_NUMBER () OVER () AS PriorityId
					FROM(
						SELECT CAST(regexp_split_to_table(_Preferences, ',') AS INT) AS ProfessorId
					) T;
			END IF;
			
			INSERT INTO public.Activity
			(AccountId, StudentId, TypeId)
			SELECT
			_AccountId, _StudentId, 3001;
			
		END IF;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT UpdatePreferences('', 1, 1);