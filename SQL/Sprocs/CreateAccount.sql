CREATE OR REPLACE FUNCTION CreateAccount(
	_Email TEXT,
	_Password TEXT
)
  RETURNS TABLE (
	  ErrorMessage TEXT
  )
  AS
  $BODY$
  DECLARE
  _AccountExists BOOL;
  _TypeId INT;
  _ContactId INT;
  _AccountId INT;
      BEGIN
	  
	  	_AccountExists := CASE WHEN EXISTS(SELECT Id FROM public.Account WHERE Username = _Email LIMIT 1) THEN TRUE ELSE FALSE END;
	  	    SELECT CASE WHEN (S.Id IS NOT NULL) THEN 300 WHEN (P.Id IS NOT NULL) THEN 200 END,
		    C.Id
			FROM
			public.Contact C
			LEFT JOIN public.Student S ON S.ContactId = C.Id
			LEFT JOIN public.Professor P ON P.ContactId = C.Id
			WHERE C.Email = _Email
			INTO _TypeId, _ContactId ;
		
		IF _AccountExists = TRUE THEN
			RETURN QUERY
			SELECT 'Korisnički račun sa ovom e-mail adresom je već registriran.';
		ELSE
	  
			 INSERT INTO public.Account
			(Username, Password, ContactId, TypeId)
			SELECT C.Email,
				_Password,
				_ContactId,
				_TypeId
			FROM
			public.Contact C
			LEFT JOIN public.Student S ON S.ContactId = C.Id
			LEFT JOIN public.Professor P ON P.ContactId = C.Id
			WHERE C.Email = _Email
			RETURNING id INTO _AccountId;
			
			IF _TypeId = 200 THEN
				UPDATE public.Professor SET
				AccountId = _AccountId,
				DateUpdated = CURRENT_TIMESTAMP
				WHERE ContactId = _ContactId;
			ELSIF _TypeId = 300 THEN
				UPDATE public.Student SET
				AccountId = _AccountId,
				DateUpdated = CURRENT_TIMESTAMP
				WHERE ContactId = _ContactId;
			END IF;
			
			RETURN QUERY
			SELECT '';
		
		END IF;

      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM CreateAccount('btopic@tvz.hr', 'encryptedpassword');