CREATE OR REPLACE FUNCTION RejectStudent(
	_StudentId INT,
	_ProfessorId INT,
	_AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE _TypeId INT;

      BEGIN
	  
	  _TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  INSERT INTO
	  public.Reject
	  (StudentId, ProfessorId)
	  SELECT _StudentId,
	    _ProfessorId
		FROM public.Professor P
		WHERE P.Id = _ProfessorId 
		AND (_TypeId = 100 OR P.AccountId = _AccountId);
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT RejectStudent(1,1,1);