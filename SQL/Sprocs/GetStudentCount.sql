CREATE OR REPLACE FUNCTION GetStudentCount(
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(S.Id) AS INT)
		  FROM public.Student S
		  INNER JOIN public.Contact C ON S.ContactId = C.Id;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetStudentCount();

-- SELECT * FROM Regenerate(35,5);