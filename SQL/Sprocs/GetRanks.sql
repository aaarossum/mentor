CREATE OR REPLACE FUNCTION GetRanks()
   RETURNS TABLE (
   		_StudentId INT,
		_ProfessorId INT,
	    _PriorityId INT,
	    _AverageGrade DECIMAL,
	    _OwnChoice BOOL
   )
   AS 
   $BODY$
	DECLARE 
	_Count INT = 0;
	_MaxStudents INT = 10;
BEGIN
	
	_MaxStudents := MaxStudents FROM public.Config LIMIT 1;
	
	DROP TABLE IF EXISTS T_Rank;
	
	CREATE TEMP TABLE T_Rank(
		Id SERIAL PRIMARY KEY,
   		StudentId INT,
		ProfessorId INT,
		PriorityId INT,
		AverageGrade DECIMAL,
		OwnChoice BOOL
	);
	
	INSERT INTO T_Rank
	(StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice)
	SELECT StudentId, 
		ProfessorId, 
		PriorityId,
		AverageGrade,
	 	OwnChoice
	FROM
	public.RankList
	WHERE IsRejected = FALSE;

	LOOP
		DELETE FROM T_Rank
		WHERE Id IN 
		(
		SELECT Id
		FROM
		(
		SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, RankId, Id, ROW_NUMBER () OVER ( PARTITION BY StudentId ORDER BY PriorityId, RankId, ProfessorEquality, AverageGrade DESC ) AS Duplicate
			FROM
			(
			SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, RankId, Id, ROW_NUMBER () OVER ( PARTITION BY StudentId ORDER BY StudentDuplicate DESC ) AS ProfessorEquality
				FROM
				(
				SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, RankId, Id, ROW_NUMBER () OVER ( PARTITION BY StudentId ORDER BY PriorityId, RankId, AverageGrade DESC ) AS StudentDuplicate
					FROM
					(
					SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id, ROW_NUMBER () OVER ( PARTITION BY ProfessorId ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC) AS RankId
						FROM
						(
							SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id
							FROM
							T_Rank
						) T
					) X
					WHERE RankId <= _MaxStudents
				) V
			) N
		) Y
		WHERE Duplicate > 1
		);
		GET DIAGNOSTICS _Count = ROW_COUNT;
		
		IF (_Count = 0) THEN
		
			DELETE FROM T_Rank
			WHERE Id IN 
			(
			SELECT Id
			FROM
			(
			SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id,
				ROW_NUMBER () OVER 
					(
					PARTITION BY ProfessorId
					ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC
					) AS RankId
			FROM
			(
			SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id
			FROM
			T_Rank
			) T
			) X
			WHERE RankId > _MaxStudents
			);
		
	  		EXIT;
	  	END IF;
		
    END LOOP;
	
   RETURN QUERY
   SELECT StudentId,
   		ProfessorId,
		PriorityId,
		AverageGrade,
	 	OwnChoice
   FROM T_Rank;
END; 

$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100
 
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- select * from GetRanks(); 