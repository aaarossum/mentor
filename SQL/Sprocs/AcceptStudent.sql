CREATE OR REPLACE FUNCTION AcceptStudent(
	_StudentId INT,
	_ProfessorId INT,
	_AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE _TypeId INT;

      BEGIN
	  
	  _TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  DELETE FROM 
	  public.Reject
	  WHERE Id IN
	  (
	  SELECT R.Id
	  FROM public.Reject R
	  INNER JOIN public.Professor P ON R.ProfessorId = P.Id
	  LEFT JOIN public.Account A ON A.Id = P.AccountId
	    WHERE R.ProfessorId = _ProfessorId 
		AND R.StudentId = _StudentId
		AND (_TypeId = 100 OR P.AccountId = _AccountId)
	  );
	  
      END  
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT AcceptStudent(1,1,1);