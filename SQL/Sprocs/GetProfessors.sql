CREATE OR REPLACE FUNCTION GetProfessors(
    _PageId INT,
	_PageSize INT
)
  RETURNS TABLE (
	  RowId BIGINT,
	  Id INT,
	  AccountId INT,
	  IsMentor BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  StudentCount BIGINT,
	  PreferenceCount BIGINT,
	  RejectionsCount BIGINT
  )
  AS
  $BODY$
      BEGIN
	  
	  	DROP TABLE IF EXISTS T_Preference;
	
		CREATE TEMP TABLE T_Preference(
			Id SERIAL PRIMARY KEY,
			ProfessorId INT,
			PreferenceCount BIGINT
		);
		
		INSERT INTO T_Preference
	  	(ProfessorId, PreferenceCount)
	  	 SELECT P.ProfessorId,
		 	COUNT(*) AS StudentCount
		 FROM
		 public.Preference P
		 GROUP BY P.ProfessorId;
		 
		DROP TABLE IF EXISTS T_Rejections;
	
		CREATE TEMP TABLE T_Rejections(
			Id SERIAL PRIMARY KEY,
			ProfessorId INT,
			RejectionsCount BIGINT
		);
		
		INSERT INTO T_Rejections
	  	(ProfessorId, RejectionsCount)
	  	 SELECT R.ProfessorId,
		 	COUNT(*) AS StudentCount
		 FROM
		 public.Reject R
		 GROUP BY R.ProfessorId;
	  
	  	  RETURN QUERY
		  SELECT T._RowId,
		  	  T.Id,
			  T.AccountId,
			  T.IsMentor,
			  T.DateCreated,
			  T.DateUpdated,
			  T.Email,
			  T.FirstName,
			  T.LastName,
			  T.Prefix,
			  T.Suffix,
			  T.JMBAG,
			  T.Phone,
			  T.JobDescr,
			  T.VPN,
			  T.StudentCount,
			  PR.PreferenceCount,
			  RJ.RejectionsCount
		  FROM
		  (
	      SELECT ROW_NUMBER () OVER (
			  ORDER BY P.Id DESC 
		   ) AS _RowId,
		      P.Id,
			  P.AccountId,
			  P.IsMentor,
			  P.DateCreated,
			  P.DateUpdated,
			  C.Email,
			  C.FirstName,
			  C.LastName,
			  C.Prefix,
			  C.Suffix,
			  C.JMBAG,
			  C.Phone,
			  C.JobDescr,
			  C.VPN,
			  COUNT(R._ProfessorId) AS StudentCount
		  FROM public.Professor P
		  INNER JOIN public.Contact C ON P.ContactId = C.Id
		  LEFT JOIN public.GetRanks() R ON R._ProfessorId = P.Id
		  GROUP BY P.Id, C.Id
          ORDER BY C.LastName
		  LIMIT (_PageSize * _PageId)
		  ) T
		  LEFT JOIN T_Preference PR ON PR.ProfessorId = T.Id
		  LEFT JOIN T_Rejections RJ ON RJ.ProfessorId = T.Id
		  WHERE _RowId > ((_PageId - 1) * _PageSize);
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetProfessors(1, 10);