CREATE OR REPLACE FUNCTION GetPreliminaryList(
	_ProfessorIdx INT,
	_PageId INT,
	_PageSize INT
)
  RETURNS TABLE (
	  RowId BIGINT,
	  RankId BIGINT,
	  StudentId INT,
	  PriorityId INT,
	  AverageGrade DECIMAL,
	  OwnChoice BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  AccountId INT,
      Username TEXT,
      TypeId INT,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  ActivityDescr TEXT,
	  ActivityDate TIMESTAMP,
	  Qualified BOOL,
	  IsRejected BOOL
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
		SELECT * 
		FROM
		(
		SELECT ROW_NUMBER () OVER (
			  ORDER BY S.Id DESC 
		   ) AS _RowId,
		   R.RankId,
			R.StudentId, 
			R.PriorityId, 
			R.AverageGrade, 
			R.OwnChoice, 
		  	S.DateCreated,
			S.DateUpdated,
		  	A.Id AS AccountId,
		  	A.Username,
		  	A.TypeId,
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN,
			LA.Descr AS ActivityDescr,
			LA.DateCreated AS ActivityDate,
			CASE WHEN (FR._professorid = _ProfessorIdx) THEN TRUE ELSE FALSE END AS Qualified,
			R.IsRejected
		FROM
		public.RankList R
		INNER JOIN public.Student S ON R.StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		LEFT JOIN public.Account A ON S.AccountId = A.Id
		LEFT JOIN public.LastStudentActivity LA ON LA.StudentId = S.Id
		LEFT JOIN public.GetRanks() FR ON FR._studentid = S.Id
		WHERE ProfessorId = _ProfessorIdx
		ORDER BY RankId
		LIMIT (_PageSize * _PageId)
	    ) T
	    WHERE _RowId > ((_PageId - 1) * _PageSize);
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetPreliminaryList(1,2,10);