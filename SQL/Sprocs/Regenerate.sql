CREATE OR REPLACE FUNCTION Regenerate(
	_StudentCount INT,
	_ProfessorCount INT,
	_Preferences BOOL,
	_AccountId INT
)
  RETURNS void 
  AS
  $BODY$
  DECLARE
  	_CourseCount INT;
	_PreferenceCount INT;
	_StudentId INT;
	_TypeId INT;
	rec_student   RECORD;
    cur_students CURSOR 
       FOR SELECT Id
       FROM public.Student;
	
      BEGIN
	  
	  	_TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  	IF _TypeId = 100 THEN
		
			DELETE FROM public.Grade;
			DELETE FROM public.Preference;
			DELETE FROM public.Activity;
			DELETE FROM public.Reject;
			DELETE FROM public.Student WHERE Id > 1;
			DELETE FROM public.Professor WHERE Id > 1;
			DELETE FROM public.Account WHERE Id > 3;
			DELETE FROM public.Contact WHERE Id > 3;

			PERFORM public.GenerateStudents(_StudentCount);
			PERFORM public.GenerateProfessors(_ProfessorCount);

			DROP TABLE IF EXISTS T_Course;

			CREATE TEMP TABLE T_Course(
				Id SERIAL PRIMARY KEY,
				CourseId INT,
				ProfessorId INT
			);

			INSERT INTO T_Course
			(CourseId, ProfessorId)
			SELECT DISTINCT ON (C.Id) C.Id, P.Id
			FROM public.Course C
			LEFT JOIN public.Professor P ON 1 = 1
			ORDER BY C.Id, random(); 

			OPEN cur_students;

			LOOP
				FETCH cur_students INTO rec_student;
				EXIT WHEN NOT FOUND;

				_CourseCount := floor(random()*(25-12+1))+12;
				_PreferenceCount := floor(random()*(4+1));

				INSERT INTO public.Grade
				(
				StudentId,
				CourseId,
				ProfessorId,
				Grade
				)
				SELECT * 
				FROM
					(SELECT rec_student.Id,
						C.CourseId,
						C.ProfessorId,
						CAST(ROUND(CAST(random()*(4-2.4+1)+2.4 AS NUMERIC),2) AS DECIMAL) AS Grade
					FROM T_Course C) T
				ORDER BY random()	
				LIMIT _CourseCount;
				
				IF _Preferences = TRUE THEN
				INSERT INTO public.Preference
				(
				 StudentId,
				 ProfessorId,
				 PriorityId
				)
				SELECT 
				StudentId,
				ProfessorId,
				PriorityId
				FROM
				(
				SELECT 
				StudentId,
				ProfessorId,
				ROW_NUMBER () OVER () AS PriorityId,
				ROW_NUMBER () OVER (PARTITION BY ProfessorId) AS ProfessorDuplicate
				FROM
					(
					 SELECT  ProfessorId,
						StudentId
					 FROM
					 public.Grade
					 WHERE StudentId = rec_student.Id
					 ORDER BY random()
					 LIMIT _PreferenceCount
					 ) T
				)X
				WHERE ProfessorDuplicate = 1;
				END IF;
			END LOOP;
		END IF;

		-- add numbers to duplicate emails
		WITH T AS (
		  SELECT Id AS DuplicateId, ROW_NUMBER () OVER (PARTITION BY Email ORDER BY Id) AS Duplicate 
		  FROM public.Contact
		)
		UPDATE public.Contact
		SET Email = split_part(Email, '@', 1) || CAST(T.Duplicate AS TEXT) || '@' || split_part(Email, '@', 2)
		FROM t
		WHERE Id = T.DuplicateId
			AND T.Duplicate > 1;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

--SELECT * FROM Regenerate(100,10,TRUE,1);

--SELECT * FROM Regenerate(10,10,FALSE,1);