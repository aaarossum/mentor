CREATE TABLE Preference(
   Id SERIAL PRIMARY KEY,
   StudentId INT REFERENCES Student(Id),
   PriorityId INT,
   ProfessorId INT REFERENCES Professor(Id),
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Preference TO webusers;