CREATE TABLE Student (
	Id SERIAL PRIMARY KEY,
    ContactId INT REFERENCES Contact(Id),
    AccountId INT REFERENCES Account(Id),
    DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Student TO webusers;