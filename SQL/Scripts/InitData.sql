INSERT INTO public.Config (MaxStudents) 
SELECT 10;

INSERT INTO Name (Descr)
SELECT 'Mia'
UNION ALL
SELECT 'Sara'
UNION ALL
SELECT 'Lucija'
UNION ALL
SELECT 'Ema'
UNION ALL
SELECT 'Ana'
UNION ALL
SELECT 'Nika'
UNION ALL
SELECT 'Petra'
UNION ALL
SELECT 'Marta'
UNION ALL
SELECT 'Lana'
UNION ALL
SELECT 'Rita'
UNION ALL
SELECT 'Elena'
UNION ALL
SELECT 'Iva'
UNION ALL
SELECT 'Lea'
UNION ALL
SELECT 'Eva'
UNION ALL
SELECT 'Lara'
UNION ALL
SELECT 'Marija'
UNION ALL
SELECT 'Dora'
UNION ALL
SELECT 'Laura'
UNION ALL
SELECT 'Klara'
UNION ALL
SELECT 'Mila'
UNION ALL
SELECT 'Leona'
UNION ALL
SELECT 'Tena'
UNION ALL
SELECT 'Ena'
UNION ALL
SELECT 'Hana'
UNION ALL
SELECT 'Franka'
UNION ALL
SELECT 'Lena'
UNION ALL
SELECT 'Karla'
UNION ALL
SELECT 'Katja'
UNION ALL
SELECT 'Maša'
UNION ALL
SELECT 'Lorena'
UNION ALL
SELECT 'Magdalena'
UNION ALL
SELECT 'Katarina'
UNION ALL
SELECT 'Tia'
UNION ALL
SELECT 'Tara'
UNION ALL
SELECT 'Helena'
UNION ALL
SELECT 'Vita'
UNION ALL
SELECT 'Sofia'
UNION ALL
SELECT 'Nora'
UNION ALL
SELECT 'Tea'
UNION ALL
SELECT 'Nikol'
UNION ALL
SELECT 'Nina'
UNION ALL
SELECT 'Una'
UNION ALL
SELECT 'Ella'
UNION ALL
SELECT 'Ela'
UNION ALL
SELECT 'Bruna'
UNION ALL
SELECT 'Matea'
UNION ALL
SELECT 'Paula'
UNION ALL
SELECT 'Doris'
UNION ALL
SELECT 'Gabriela'
UNION ALL
SELECT 'Jana'
UNION ALL
SELECT 'Luka'
UNION ALL
SELECT 'David'
UNION ALL
SELECT 'Ivan'
UNION ALL
SELECT 'Jakov'
UNION ALL
SELECT 'Petar'
UNION ALL
SELECT 'Filip'
UNION ALL
SELECT 'Marko'
UNION ALL
SELECT 'Matej'
UNION ALL
SELECT 'Mihael'
UNION ALL
SELECT 'Mateo'
UNION ALL
SELECT 'Leon'
UNION ALL
SELECT 'Josip'
UNION ALL
SELECT 'Karlo'
UNION ALL
SELECT 'Ivano'
UNION ALL
SELECT 'Fran'
UNION ALL
SELECT 'Noa'
UNION ALL
SELECT 'Roko'
UNION ALL
SELECT 'Borna'
UNION ALL
SELECT 'Jan'
UNION ALL
SELECT 'Niko'
UNION ALL
SELECT 'Gabriel'
UNION ALL
SELECT 'Ante'
UNION ALL
SELECT 'Lovro'
UNION ALL
SELECT 'Toma'
UNION ALL
SELECT 'Lukas'
UNION ALL
SELECT 'Vito'
UNION ALL
SELECT 'Nikola'
UNION ALL
SELECT 'Teo'
UNION ALL
SELECT 'Leo'
UNION ALL
SELECT 'Dominik'
UNION ALL
SELECT 'Marin'
UNION ALL
SELECT 'Patrik'
UNION ALL
SELECT 'Šimun'
UNION ALL
SELECT 'Tin'
UNION ALL
SELECT 'Toni'
UNION ALL
SELECT 'Matija'
UNION ALL
SELECT 'Antonio'
UNION ALL
SELECT 'Emanuel'
UNION ALL
SELECT 'Bruno'
UNION ALL
SELECT 'Andrej'
UNION ALL
SELECT 'Viktor'
UNION ALL
SELECT 'Erik'
UNION ALL
SELECT 'Adrian'
UNION ALL
SELECT 'Noel'
UNION ALL
SELECT 'Rafael'
UNION ALL
SELECT 'Duje'
UNION ALL
SELECT 'Gabrijel'
UNION ALL
SELECT 'Lovre'
UNION ALL
SELECT 'Martin'
UNION ALL
SELECT 'Dino';

INSERT INTO Surname (Descr)
SELECT 'Babić'
UNION ALL
SELECT 'Barić'
UNION ALL
SELECT 'Barišić'
UNION ALL
SELECT 'Bačić'
UNION ALL
SELECT 'Bašić'
UNION ALL
SELECT 'Bilić'
UNION ALL
SELECT 'Blažević'
UNION ALL
SELECT 'Bogdan'
UNION ALL
SELECT 'Bošnjak'
UNION ALL
SELECT 'Božić'
UNION ALL
SELECT 'Brajković'
UNION ALL
SELECT 'Brkić'
UNION ALL
SELECT 'Burić'
UNION ALL
SELECT 'Cindrić'
UNION ALL
SELECT 'Delić'
UNION ALL
SELECT 'Dujmović'
UNION ALL
SELECT 'Filipović'
UNION ALL
SELECT 'Galić'
UNION ALL
SELECT 'Grgić'
UNION ALL
SELECT 'Grubišić'
UNION ALL
SELECT 'Herceg'
UNION ALL
SELECT 'Horvat'
UNION ALL
SELECT 'Ilić'
UNION ALL
SELECT 'Ivanković'
UNION ALL
SELECT 'Ivanović'
UNION ALL
SELECT 'Ivančić'
UNION ALL
SELECT 'Jakšić'
UNION ALL
SELECT 'Janković'
UNION ALL
SELECT 'Jelić'
UNION ALL
SELECT 'Jerković'
UNION ALL
SELECT 'Jovanović'
UNION ALL
SELECT 'Jozić'
UNION ALL
SELECT 'Jukić'
UNION ALL
SELECT 'Jurić'
UNION ALL
SELECT 'Jurišić'
UNION ALL
SELECT 'Jurković'
UNION ALL
SELECT 'Katić'
UNION ALL
SELECT 'Klarić'
UNION ALL
SELECT 'Knežević'
UNION ALL
SELECT 'Kolar'
UNION ALL
SELECT 'Kos'
UNION ALL
SELECT 'Kovač'
UNION ALL
SELECT 'Kovačević'
UNION ALL
SELECT 'Kovačić'
UNION ALL
SELECT 'Kralj'
UNION ALL
SELECT 'Lončar'
UNION ALL
SELECT 'Lovrić'
UNION ALL
SELECT 'Lukić'
UNION ALL
SELECT 'Lučić'
UNION ALL
SELECT 'Mandić'
UNION ALL
SELECT 'Marinović'
UNION ALL
SELECT 'Marić'
UNION ALL
SELECT 'Marjanović'
UNION ALL
SELECT 'Marković'
UNION ALL
SELECT 'Martinović'
UNION ALL
SELECT 'Marušić'
UNION ALL
SELECT 'Matijević'
UNION ALL
SELECT 'Matić'
UNION ALL
SELECT 'Matković'
UNION ALL
SELECT 'Mihaljević'
UNION ALL
SELECT 'Mikulić'
UNION ALL
SELECT 'Miletić'
UNION ALL
SELECT 'Milić'
UNION ALL
SELECT 'Nikolić'
UNION ALL
SELECT 'Novak'
UNION ALL
SELECT 'Novaković'
UNION ALL
SELECT 'Novosel'
UNION ALL
SELECT 'Pavić'
UNION ALL
SELECT 'Pavičić'
UNION ALL
SELECT 'Pavlović'
UNION ALL
SELECT 'Pejić'
UNION ALL
SELECT 'Perić'
UNION ALL
SELECT 'Perković'
UNION ALL
SELECT 'Petković'
UNION ALL
SELECT 'Petrić'
UNION ALL
SELECT 'Petrović'
UNION ALL
SELECT 'Poljak'
UNION ALL
SELECT 'Popović'
UNION ALL
SELECT 'Posavec'
UNION ALL
SELECT 'Pranjić'
UNION ALL
SELECT 'Radić'
UNION ALL
SELECT 'Rukavina'
UNION ALL
SELECT 'Stanić'
UNION ALL
SELECT 'Tadić'
UNION ALL
SELECT 'Tomić'
UNION ALL
SELECT 'Topić'
UNION ALL
SELECT 'Varga'
UNION ALL
SELECT 'Vidaković'
UNION ALL
SELECT 'Vidović'
UNION ALL
SELECT 'Vukelić'
UNION ALL
SELECT 'Vuković'
UNION ALL
SELECT 'Vučković'
UNION ALL
SELECT 'Ćosić'
UNION ALL
SELECT 'Đurić'
UNION ALL
SELECT 'Šarić'
UNION ALL
SELECT 'Šimić'
UNION ALL
SELECT 'Šimunović'
UNION ALL
SELECT 'Špoljarić'
UNION ALL
SELECT 'Štimac'
UNION ALL
SELECT 'Živković';

INSERT INTO Course
(Descr)
SELECT '3D modeliranje'
UNION ALL
SELECT 'Administriranje i poslovanje mrežama računala'
UNION ALL
SELECT 'Administriranje UNIX sustava'
UNION ALL
SELECT 'Baze podataka'
UNION ALL
SELECT 'Digitalna animacija'
UNION ALL
SELECT 'Digitalna fotografija'
UNION ALL
SELECT 'Digitalna televizija'
UNION ALL
SELECT 'Dizajn i primjena vektorske grafike'
UNION ALL
SELECT 'Dizajn i vizualno značenje'
UNION ALL
SELECT 'Dizajn proizvoda'
UNION ALL
SELECT 'Dizajn vizualnih komunikacija'
UNION ALL
SELECT 'Društvene mreže'
UNION ALL
SELECT 'Elektroničko poslovanje'
UNION ALL
SELECT 'Engleski jezik za IT'
UNION ALL
SELECT 'Fizika'
UNION ALL
SELECT 'Građa računala'
UNION ALL
SELECT 'Grafičke tehnike'
UNION ALL
SELECT 'Grafički dizajn'
UNION ALL
SELECT 'Grafički programski jezici'
UNION ALL
SELECT 'Informacijska pismenost i kritičko razmišljanje'
UNION ALL
SELECT 'Inovacije u informatici'
UNION ALL
SELECT 'Instalacija računala i programa'
UNION ALL
SELECT 'Integracija medija'
UNION ALL
SELECT 'Interaktivno programiranje na Web-u'
UNION ALL
SELECT 'Interaktivno programiranje na Web-u'
UNION ALL
SELECT 'Kineziološka kultura I'
UNION ALL
SELECT 'Kineziološka kultura II'
UNION ALL
SELECT 'Kineziološka kultura III'
UNION ALL
SELECT 'Kineziološka kultura IV'
UNION ALL
SELECT 'Komunikacijski sustavi i mreže'
UNION ALL
SELECT 'Matematika I'
UNION ALL
SELECT 'Matematika II'
UNION ALL
SELECT 'Mobilne komunikacije'
UNION ALL
SELECT 'Multimedijski marketing'
UNION ALL
SELECT 'Napredne baze podataka'
UNION ALL
SELECT 'Napredne tehnologije interneta'
UNION ALL
SELECT 'Napredno elektroničko poslovanje'
UNION ALL
SELECT 'Njemački jezik za IT'
UNION ALL
SELECT 'Objektno orijentirano programiranje I'
UNION ALL
SELECT 'Objektno orijentirano programiranje II'
UNION ALL
SELECT 'Oblikovanje e literature'
UNION ALL
SELECT 'Oblikovanje Web stranica'
UNION ALL
SELECT 'Obrada slike, zvuka i videa'
UNION ALL
SELECT 'Obrada slike, zvuka i videa'
UNION ALL
SELECT 'Obrada teksta'
UNION ALL
SELECT 'Operacijski sustavi'
UNION ALL
SELECT 'Organizacija i informatizacija ureda'
UNION ALL
SELECT 'Osnove programiranja'
UNION ALL
SELECT 'Poslovni engleski jezik za IT'
UNION ALL
SELECT 'Poslovni njemački jezik za IT'
UNION ALL
SELECT 'Praktikum iz dizajna'
UNION ALL
SELECT 'Pretražnici i navigacija na Web-u'
UNION ALL
SELECT 'Procesi video produkcije'
UNION ALL
SELECT 'Produkcija zvuka'
UNION ALL
SELECT 'Programiranje'
UNION ALL
SELECT 'Programiranje web aplikacija'
UNION ALL
SELECT 'Projektno programiranje'
UNION ALL
SELECT 'Računalna grafika'
UNION ALL
SELECT 'Računalna tipografija'
UNION ALL
SELECT 'Razvoj računalnih igara'
UNION ALL
SELECT 'Reprofotografija'
UNION ALL
SELECT 'Sigurnost i zaštita informacijskih sustava'
UNION ALL
SELECT 'Socio tehnološki aspekti Informacijskih sustava'
UNION ALL
SELECT 'Stručna praksa'
UNION ALL
SELECT 'Sustavi elektroničkog poslovanja'
UNION ALL
SELECT 'Tablični kalkulatori'
UNION ALL
SELECT 'Tehnološko poduzetništvo'
UNION ALL
SELECT 'Tehnološko poduzetništvo'
UNION ALL
SELECT 'Teorija i razvoj dizajna'
UNION ALL
SELECT 'Tržišne komunikacije'
UNION ALL
SELECT 'TV i Video snimanje'
UNION ALL
SELECT 'Uredsko poslovanje'
UNION ALL
SELECT 'Uvod u (X)HTML i CSS'
UNION ALL
SELECT 'Uvod u mreže računala'
UNION ALL
SELECT 'Uvod u UNIX sustave'
UNION ALL
SELECT 'Vjerojatnost i statistika'
UNION ALL
SELECT 'Vještine komuniciranja'
UNION ALL
SELECT 'XML programiranje';


INSERT INTO AccountType (Id, Descr)
SELECT 100, 'Admin'
UNION ALL
SELECT 200, 'Professor'
UNION ALL
SELECT 300, 'Student'
;
--select * from AccountType;

INSERT INTO ActivityType (Id, Descr)
SELECT 2001, 'Odbijanje ili prihvaćanje studenta'
UNION ALL
SELECT 3001, 'Izmjena preference mentora'
;

-- generate students and professors
SELECT * FROM Regenerate(100,10,TRUE,1);