-- CREATE ADMIN ACCOUNT ---------------
INSERT INTO Contact (Email, FirstName, LastName)
SELECT 'admin@admin.com', 'Admin', 'Test';

INSERT INTO Account (Username, Password, TypeId, ContactId)
SELECT 'admin@admin.com', '$2b$12$lyOKfPHfczfgznz7uL0IoOTlK/FG9S0cIzHyMj.O.kWQjbrUSA2c6', 100, 1;
-- bcrypt: "sifra123"
--select * from Account;

-- CREATE PROFESSOR ACCOUNT -----------------
INSERT INTO Contact (Email, FirstName, LastName, Prefix, Suffix)
SELECT 'ikolar@tvz.hr', 'Ivan', 'Kolar', 'dr. sc.', 'viši predavač';

INSERT INTO Account (Username, Password, TypeId, ContactId)
SELECT 'ikolar@tvz.hr', '$2b$12$lyOKfPHfczfgznz7uL0IoOTlK/FG9S0cIzHyMj.O.kWQjbrUSA2c6', 200, 2;
-- bcrypt: "sifra123"
--select * from Account;

INSERT INTO Professor (ContactId, AccountId)
SELECT 2,2;

-- CREATE STUDENT ACCOUNT -----------------
INSERT INTO Contact (Email, FirstName, LastName, JMBAG)
SELECT 'lnovak@tvz.hr', 'Luka', 'Novak', '0249080843';

INSERT INTO Account (Username, Password, TypeId, ContactId)
SELECT 'lnovak@tvz.hr', '$2b$12$lyOKfPHfczfgznz7uL0IoOTlK/FG9S0cIzHyMj.O.kWQjbrUSA2c6', 300, 3;
-- bcrypt: "sifra123"
--select * from Account;

INSERT INTO Student (ContactId, AccountId)
SELECT 3,3;