-------------------- TABLES -------------------------
CREATE TABLE Contact (
	Id SERIAL PRIMARY KEY,
    Email TEXT NOT NULL,
    FirstName TEXT NOT NULL,
    LastName TEXT NOT NULL,
    Prefix TEXT,
    Suffix TEXT,
    JMBAG TEXT,
    Phone TEXT,
    JobDescr TEXT,
    VPN INT,
    DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Contact TO webusers;

CREATE TABLE AccountType (
	Id INT PRIMARY KEY,
    Descr TEXT NOT NULL,
    DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON AccountType TO webusers;

CREATE TABLE Account (
  Id SERIAL PRIMARY KEY,
  Username TEXT NOT NULL,
  Password TEXT NOT NULL,
  ContactId INT REFERENCES Contact(Id) NOT NULL,
  TypeId INT REFERENCES AccountType(Id) NOT NULL,
  DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(Username)
);
GRANT SELECT ON Account TO webusers;

CREATE TABLE Student (
	Id SERIAL PRIMARY KEY,
    ContactId INT REFERENCES Contact(Id),
    AccountId INT REFERENCES Account(Id),
    DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Student TO webusers;

CREATE TABLE Professor (
	Id SERIAL PRIMARY KEY,
    ContactId INT REFERENCES Contact(Id),
    AccountId INT REFERENCES Account(Id),
    IsMentor BOOL DEFAULT TRUE,
    DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Professor TO webusers;

CREATE TABLE Course (
	Id SERIAL PRIMARY KEY,
    Descr TEXT,
    DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Course TO webusers;

CREATE TABLE Grade(
   Id SERIAL PRIMARY KEY,
   StudentId INT REFERENCES Student(Id),
   CourseId INT REFERENCES Course(Id),
   ProfessorId INT REFERENCES Professor(Id),
   Grade DECIMAL,
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Grade TO webusers;

CREATE TABLE Preference(
   Id SERIAL PRIMARY KEY,
   StudentId INT REFERENCES Student(Id),
   PriorityId INT,
   ProfessorId INT REFERENCES Professor(Id),
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Preference TO webusers;

CREATE TABLE ActivityType(
   Id INT PRIMARY KEY,
   Descr TEXT,
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON ActivityType TO webusers;

CREATE TABLE Activity(
   Id SERIAL PRIMARY KEY,
   AccountId INT REFERENCES Account(Id),
   StudentId INT REFERENCES Student(Id),
   TypeId INT REFERENCES ActivityType(Id),
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Activity TO webusers;

CREATE TABLE Config(
   Id SERIAL PRIMARY KEY,
   MaxStudents INT,
   DateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Config TO webusers;

CREATE TABLE Reject(
   Id SERIAL PRIMARY KEY,
   ProfessorId INT REFERENCES Professor(Id),
   StudentId INT REFERENCES Student(Id),
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Reject TO webusers;

CREATE TABLE Verification(
   Id SERIAL PRIMARY KEY,
   Email TEXT,
   Code TEXT,
   DateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
GRANT SELECT ON Verification TO webusers;

CREATE TABLE Name(
    Id SERIAL PRIMARY KEY,
    Descr TEXT
);

CREATE TABLE Surname(
    Id SERIAL PRIMARY KEY,
    Descr TEXT
);

------ generate students -----------------------------------

CREATE OR REPLACE FUNCTION GenerateStudents(
	_Count INT DEFAULT 5
)
  RETURNS void 
  AS
  $BODY$
  DECLARE
  _JMBAG TEXT;
  _Email TEXT;
  _Name TEXT;
  _Surname TEXT;
  _i INT := 0;
      BEGIN
	  
	  	LOOP
			EXIT WHEN _i = _Count; 
      		_i := _i + 1;
			
			_Name := Descr FROM public.Name 
			OFFSET floor(random() * (SELECT COUNT(*) FROM public.Name)) LIMIT 1;

			_Surname := Descr FROM public.Surname 
			OFFSET floor(random() * (SELECT COUNT(*) FROM public.Surname)) LIMIT 1;

			_Email := LEFT(LOWER(_Name),1) || LOWER(_Surname) || '@tvz.hr';
			
			_Email := REPLACE(_Email, 'ć', 'c');
			_Email := REPLACE(_Email, 'č', 'c');
			_Email := REPLACE(_Email, 'đ', 'd');
			_Email := REPLACE(_Email, 'š', 's');
			_Email := REPLACE(_Email, 'ž', 'z');

			_JMBAG := '0249' || CAST(floor(random()*(999999-100000+1))+100000 AS TEXT);

			WITH NewContact AS(
				INSERT INTO public.Contact (Email, FirstName, LastName, JMBAG)
				SELECT _Email, _Name, _Surname, _JMBAG
				RETURNING Id
			)
			INSERT INTO public.Student (ContactId)
			SELECT Id FROM NewContact;
			
		END LOOP;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

--SELECT * FROM GenerateStudents(100);

CREATE OR REPLACE FUNCTION GenerateProfessors(
	_Count INT DEFAULT 5
)
  RETURNS void 
  AS
  $BODY$
  DECLARE
  _Email TEXT;
  _Name TEXT;
  _Surname TEXT;
  _i INT := 0;
      BEGIN
	  
	  	LOOP
			EXIT WHEN _i = _Count; 
      		_i := _i + 1;
			
			_Name := Descr FROM public.Name 
			OFFSET floor(random() * (SELECT COUNT(*) FROM public.Name)) LIMIT 1;

			_Surname := Descr FROM public.Surname 
			OFFSET floor(random() * (SELECT COUNT(*) FROM public.Surname)) LIMIT 1;

			_Email := LEFT(LOWER(_Name),1) || LOWER(_Surname) || '@tvz.hr';
			
			_Email := REPLACE(_Email, 'ć', 'c');
			_Email := REPLACE(_Email, 'č', 'c');
			_Email := REPLACE(_Email, 'đ', 'd');
			_Email := REPLACE(_Email, 'š', 's');
			_Email := REPLACE(_Email, 'ž', 'z');

			WITH NewContact AS(
				INSERT INTO public.Contact (Email, FirstName, LastName, Prefix)
				SELECT _Email, _Name, _Surname, 'prof.'
				RETURNING Id
			)
			INSERT INTO public.Professor (ContactId)
			SELECT Id FROM NewContact;
			
		END LOOP;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

--SELECT * FROM GenerateProfessors();

CREATE OR REPLACE FUNCTION Regenerate(
	_StudentCount INT,
	_ProfessorCount INT,
	_Preferences BOOL,
	_AccountId INT
)
  RETURNS void 
  AS
  $BODY$
  DECLARE
  	_CourseCount INT;
	_PreferenceCount INT;
	_StudentId INT;
	_TypeId INT;
	rec_student   RECORD;
    cur_students CURSOR 
       FOR SELECT Id
       FROM public.Student;
	
      BEGIN
	  
	  	_TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  	IF _TypeId = 100 THEN
		
			DELETE FROM public.Grade;
			DELETE FROM public.Preference;
			DELETE FROM public.Activity;
			DELETE FROM public.Reject;
			DELETE FROM public.Student WHERE Id > 1;
			DELETE FROM public.Professor WHERE Id > 1;
			DELETE FROM public.Account WHERE Id > 3;
			DELETE FROM public.Contact WHERE Id > 3;

			PERFORM public.GenerateStudents(_StudentCount);
			PERFORM public.GenerateProfessors(_ProfessorCount);

			DROP TABLE IF EXISTS T_Course;

			CREATE TEMP TABLE T_Course(
				Id SERIAL PRIMARY KEY,
				CourseId INT,
				ProfessorId INT
			);

			INSERT INTO T_Course
			(CourseId, ProfessorId)
			SELECT DISTINCT ON (C.Id) C.Id, P.Id
			FROM public.Course C
			LEFT JOIN public.Professor P ON 1 = 1
			ORDER BY C.Id, random(); 

			OPEN cur_students;

			LOOP
				FETCH cur_students INTO rec_student;
				EXIT WHEN NOT FOUND;

				_CourseCount := floor(random()*(25-12+1))+12;
				_PreferenceCount := floor(random()*(4+1));

				INSERT INTO public.Grade
				(
				StudentId,
				CourseId,
				ProfessorId,
				Grade
				)
				SELECT * 
				FROM
					(SELECT rec_student.Id,
						C.CourseId,
						C.ProfessorId,
						CAST(ROUND(CAST(random()*(4-2.4+1)+2.4 AS NUMERIC),2) AS DECIMAL) AS Grade
					FROM T_Course C) T
				ORDER BY random()	
				LIMIT _CourseCount;
				
				IF _Preferences = TRUE THEN
				INSERT INTO public.Preference
				(
				 StudentId,
				 ProfessorId,
				 PriorityId
				)
				SELECT 
				StudentId,
				ProfessorId,
				PriorityId
				FROM
				(
				SELECT 
				StudentId,
				ProfessorId,
				ROW_NUMBER () OVER () AS PriorityId,
				ROW_NUMBER () OVER (PARTITION BY ProfessorId) AS ProfessorDuplicate
				FROM
					(
					 SELECT  ProfessorId,
						StudentId
					 FROM
					 public.Grade
					 WHERE StudentId = rec_student.Id
					 ORDER BY random()
					 LIMIT _PreferenceCount
					 ) T
				)X
				WHERE ProfessorDuplicate = 1;
				END IF;
			END LOOP;
		END IF;

    -- add numbers to duplicate emails
    WITH T AS (
      SELECT Id AS DuplicateId, ROW_NUMBER () OVER (PARTITION BY Email ORDER BY Id) AS Duplicate 
      FROM Contact
    )
    UPDATE public.Contact
    SET Email = split_part(Email, '@', 1) || CAST(T.Duplicate AS TEXT) || '@' || split_part(Email, '@', 2)
    FROM t
    WHERE Id = T.DuplicateId
      AND T.Duplicate > 1;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

SELECT * FROM Regenerate(100,10,TRUE,1);

--SELECT * FROM Regenerate(10,10,FALSE,1);

-------------------- SPROCS -------------------------
CREATE OR REPLACE FUNCTION GetAccount(
	_Id INT,
	_Pass BOOL DEFAULT FALSE
)
  RETURNS TABLE (
	  Id INT,
	  Username TEXT,
	  TypeId INT,
	  Password TEXT,
	  StudentId INT,
	  ProfessorId INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT A.Id,
			A.Username,
			A.TypeId,
			CASE WHEN (_Pass = TRUE) THEN A.Password ELSE '' END AS Password,
			COALESCE(S.Id,0) AS StudentId,
			COALESCE(P.Id,0) AS ProfessorId
		  FROM public.Account A
		  LEFT JOIN public.Student S ON S.AccountId = A.Id
		  LEFT JOIN public.Professor P ON P.AccountId = A.Id
		  WHERE A.Id = _Id;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetAccount(3, TRUE);

CREATE OR REPLACE FUNCTION GetStudents(
    _PageId INT,
	_PageSize INT,
	_ProfessorIdX INT DEFAULT 0
)
  RETURNS TABLE (
	  RowId BIGINT,
	  Id INTEGER,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  AccountId INT,
      Username TEXT,
      TypeId INT,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  ProfessorId INT,
	  ProfessorFirstName TEXT,
	  ProfessorLastName TEXT,
	  ProfessorPrefix TEXT,
	  ProfessorSuffix TEXT,
	  ActivityDescr TEXT,
	  ActivityDate TIMESTAMP,
	  IsRejected BOOL
  )
  AS
  $BODY$
      BEGIN
	  	  RETURN QUERY
		  SELECT * 
		  FROM
		  (
	      SELECT ROW_NUMBER () OVER (
			  ORDER BY S.Id DESC 
		   ) AS _RowId,
		  	S.Id,
		  	S.DateCreated,
			S.DateUpdated,
		  	A.Id AS AccountId,
		  	A.Username,
		  	A.TypeId,
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN,
			R._ProfessorId,
			PC.FirstName AS ProfessorFirstName,
			PC.LastName AS ProfessorLastName,
			PC.Prefix AS ProfessorPrefix,
			PC.Suffix AS ProfessorSuffix,
			LA.Descr AS ActivityDescr,
			LA.DateCreated AS ActivityDate,
			CASE WHEN (RJ.ProfessorId = _ProfessorIdX) THEN TRUE ELSE FALSE END AS IsRejected
		  FROM public.Student S
		  LEFT JOIN public.Account A ON S.AccountId = A.Id
		  INNER JOIN public.Contact C ON S.ContactId = C.Id
		  LEFT JOIN public.GetRanks() R ON R._StudentId = S.Id
		  LEFT JOIN public.Professor P ON P.Id = R._ProfessorId
		  LEFT JOIN public.Contact PC ON PC.Id = P.ContactId
		  LEFT JOIN public.LastStudentActivity LA ON LA.StudentId = S.Id
		  LEFT JOIN public.Reject RJ ON RJ.StudentId = S.Id AND RJ.ProfessorId = _ProfessorIdX
		  LIMIT (_PageSize * _PageId)
		  ) T
		  WHERE _RowId > ((_PageId - 1) * _PageSize);
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetStudents(1, 8, 1);


CREATE OR REPLACE FUNCTION GetStudentCount(
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(S.Id) AS INT)
		  FROM public.Student S
		  INNER JOIN public.Contact C ON S.ContactId = C.Id;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetStudentCount();

-- SELECT * FROM Regenerate(35,5);

CREATE OR REPLACE FUNCTION GetProfessors(
    _PageId INT,
	_PageSize INT
)
  RETURNS TABLE (
	  RowId BIGINT,
	  Id INT,
	  AccountId INT,
	  IsMentor BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  StudentCount BIGINT,
	  PreferenceCount BIGINT,
	  RejectionsCount BIGINT
  )
  AS
  $BODY$
      BEGIN
	  
	  	DROP TABLE IF EXISTS T_Preference;
	
		CREATE TEMP TABLE T_Preference(
			Id SERIAL PRIMARY KEY,
			ProfessorId INT,
			PreferenceCount BIGINT
		);
		
		INSERT INTO T_Preference
	  	(ProfessorId, PreferenceCount)
	  	 SELECT P.ProfessorId,
		 	COUNT(*) AS StudentCount
		 FROM
		 public.Preference P
		 GROUP BY P.ProfessorId;
		 
		DROP TABLE IF EXISTS T_Rejections;
	
		CREATE TEMP TABLE T_Rejections(
			Id SERIAL PRIMARY KEY,
			ProfessorId INT,
			RejectionsCount BIGINT
		);
		
		INSERT INTO T_Rejections
	  	(ProfessorId, RejectionsCount)
	  	 SELECT R.ProfessorId,
		 	COUNT(*) AS StudentCount
		 FROM
		 public.Reject R
		 GROUP BY R.ProfessorId;
	  
	  	  RETURN QUERY
		  SELECT T._RowId,
		  	  T.Id,
			  T.AccountId,
			  T.IsMentor,
			  T.DateCreated,
			  T.DateUpdated,
			  T.Email,
			  T.FirstName,
			  T.LastName,
			  T.Prefix,
			  T.Suffix,
			  T.JMBAG,
			  T.Phone,
			  T.JobDescr,
			  T.VPN,
			  T.StudentCount,
			  PR.PreferenceCount,
			  RJ.RejectionsCount
		  FROM
		  (
	      SELECT ROW_NUMBER () OVER (
			  ORDER BY P.Id DESC 
		   ) AS _RowId,
		      P.Id,
			  P.AccountId,
			  P.IsMentor,
			  P.DateCreated,
			  P.DateUpdated,
			  C.Email,
			  C.FirstName,
			  C.LastName,
			  C.Prefix,
			  C.Suffix,
			  C.JMBAG,
			  C.Phone,
			  C.JobDescr,
			  C.VPN,
			  COUNT(R._ProfessorId) AS StudentCount
		  FROM public.Professor P
		  INNER JOIN public.Contact C ON P.ContactId = C.Id
		  LEFT JOIN public.GetRanks() R ON R._ProfessorId = P.Id
		  GROUP BY P.Id, C.Id
          ORDER BY C.LastName
		  LIMIT (_PageSize * _PageId)
		  ) T
		  LEFT JOIN T_Preference PR ON PR.ProfessorId = T.Id
		  LEFT JOIN T_Rejections RJ ON RJ.ProfessorId = T.Id
		  WHERE _RowId > ((_PageId - 1) * _PageSize);
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetProfessors(1, 10);

CREATE OR REPLACE FUNCTION GetProfessorCount(
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(P.Id) AS INT)
		  FROM public.Professor P
		  INNER JOIN public.Contact C ON P.ContactId = C.Id;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetProfessorCount();

CREATE OR REPLACE FUNCTION GetProfessorsAll(
)
  RETURNS TABLE (
	  Id INT,
	  AccountId INT,
	  IsMentor BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT
  )
  AS
  $BODY$
      BEGIN
	  	  RETURN QUERY
	      SELECT P.Id,
			  P.AccountId,
			  P.IsMentor,
			  P.DateCreated,
			  P.DateUpdated,
			  C.Email,
			  C.FirstName,
			  C.LastName,
			  C.Prefix,
			  C.Suffix,
			  C.JMBAG,
			  C.Phone,
			  C.JobDescr,
			  C.VPN
		  FROM public.Professor P
		  INNER JOIN public.Contact C ON P.ContactId = C.Id
		  ORDER BY C.LastName;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetProfessorsAll();

CREATE OR REPLACE FUNCTION GetConfig(
)
  RETURNS TABLE (
	  MaxStudents INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT C.MaxStudents
		  FROM public.Config C
		  LIMIT 1;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetConfig();

CREATE OR REPLACE FUNCTION SaveConfig(
	_MaxStudents INT,
	_AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE _TypeId INT;

      BEGIN

	  _TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  IF _TypeId = 100 THEN
		  UPDATE public.Config SET
		  MaxStudents = _MaxStudents;
	  END IF;
	  
      END  
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT SaveConfig(15,1);

CREATE OR REPLACE FUNCTION GetAverageGrade(
	_StudentId INT,
    _ProfessorId INT
	)
  RETURNS DECIMAL AS
  $BODY$
  DECLARE _USD DECIMAL;
      BEGIN
		RETURN (
		SELECT COALESCE(AVG(G.Grade),0)
        FROM public.Student S
        INNER JOIN public.Contact C ON S.ContactId = C.Id
        LEFT JOIN public.Grade G ON G.StudentId = S.Id
        WHERE G.ProfessorId = _ProfessorId 
        		AND S.Id = _StudentId );
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT GetAverageGrade(1,1);

CREATE VIEW LastStudentActivity AS
    SELECT DISTINCT ON (A.StudentId) StudentId,
	A.DateCreated,
	T.Descr
    FROM public.Activity A
	INNER JOIN public.ActivityType T ON A.TypeId = T.Id
	ORDER BY A.StudentId, DateCreated DESC;
GRANT SELECT ON LastStudentActivity TO webusers;

CREATE VIEW Priority AS
    SELECT DISTINCT(PriorityId) AS Id
    FROM Preference
	ORDER BY PriorityId;
GRANT SELECT ON Priority TO webusers;

-- SELECT _StudentId, _ProfessorId FROM GetRanks();
-- SELECT * FROM Student;
-- SELECT * FROM FinalRank;
-- SELECT * FROM MasterRankList;

/*
CREATE VIEW RankList AS
SELECT S.Id AS StudentId,
	GetAverageGrade(S.Id, P.Id) AS AverageGrade,
	P.Id AS ProfessorId,
	PR.PriorityId,
	CASE WHEN PR.PriorityId IS NULL THEN False ELSE True END AS OwnChoice
FROM Professor P
CROSS JOIN Student S
LEFT JOIN Preference PR ON PR.ProfessorId = P.Id AND PR.StudentId = S.Id
ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC;*/

CREATE VIEW RankList AS
SELECT T.StudentId, T.ProfessorId, PriorityId, AverageGrade, OwnChoice,
			ROW_NUMBER () OVER 
				(
				PARTITION BY T.ProfessorId
				ORDER BY T.ProfessorId, OwnChoice DESC, AverageGrade DESC
				) AS RankId,
			CASE WHEN R.StudentId IS NULL THEN False ELSE True END AS IsRejected
		FROM
		(
			
		SELECT S.Id AS StudentId,
			GetAverageGrade(S.Id, P.Id) AS AverageGrade,
			P.Id AS ProfessorId,
			PR.PriorityId,
			CASE WHEN PR.PriorityId IS NULL THEN False ELSE True END AS OwnChoice
		FROM Professor P
		CROSS JOIN Student S
		LEFT JOIN Preference PR ON PR.ProfessorId = P.Id AND PR.StudentId = S.Id
		ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC	
		) T
		LEFT JOIN public.Reject R ON T.ProfessorId = R.ProfessorId AND T.StudentId = R.StudentId
		;
GRANT SELECT ON RankList TO webusers;


CREATE OR REPLACE FUNCTION GetRanks()
   RETURNS TABLE (
   		_StudentId INT,
		_ProfessorId INT,
	    _PriorityId INT,
	    _AverageGrade DECIMAL,
	    _OwnChoice BOOL
   )
   AS 
   $BODY$
	DECLARE 
	_Count INT = 0;
	_MaxStudents INT = 10;
BEGIN
	
	_MaxStudents := MaxStudents FROM public.Config LIMIT 1;
	
	DROP TABLE IF EXISTS T_Rank;
	
	CREATE TEMP TABLE T_Rank(
		Id SERIAL PRIMARY KEY,
   		StudentId INT,
		ProfessorId INT,
		PriorityId INT,
		AverageGrade DECIMAL,
		OwnChoice BOOL
	);
	
	INSERT INTO T_Rank
	(StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice)
	SELECT StudentId, 
		ProfessorId, 
		PriorityId,
		AverageGrade,
	 	OwnChoice
	FROM
	public.RankList
	WHERE IsRejected = FALSE;

	LOOP
		DELETE FROM T_Rank
		WHERE Id IN 
		(
		SELECT Id
		FROM
		(
		SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, RankId, Id, ROW_NUMBER () OVER ( PARTITION BY StudentId ORDER BY PriorityId, RankId, ProfessorEquality, AverageGrade DESC ) AS Duplicate
			FROM
			(
			SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, RankId, Id, ROW_NUMBER () OVER ( PARTITION BY StudentId ORDER BY StudentDuplicate DESC ) AS ProfessorEquality
				FROM
				(
				SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, RankId, Id, ROW_NUMBER () OVER ( PARTITION BY StudentId ORDER BY PriorityId, RankId, AverageGrade DESC ) AS StudentDuplicate
					FROM
					(
					SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id, ROW_NUMBER () OVER ( PARTITION BY ProfessorId ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC) AS RankId
						FROM
						(
							SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id
							FROM
							T_Rank
						) T
					) X
					WHERE RankId <= _MaxStudents
				) V
			) N
		) Y
		WHERE Duplicate > 1
		);
		GET DIAGNOSTICS _Count = ROW_COUNT;
		
		IF (_Count = 0) THEN
		
			DELETE FROM T_Rank
			WHERE Id IN 
			(
			SELECT Id
			FROM
			(
			SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id,
				ROW_NUMBER () OVER 
					(
					PARTITION BY ProfessorId
					ORDER BY ProfessorId, OwnChoice DESC, AverageGrade DESC
					) AS RankId
			FROM
			(
			SELECT StudentId, ProfessorId, PriorityId, AverageGrade, OwnChoice, Id
			FROM
			T_Rank
			) T
			) X
			WHERE RankId > _MaxStudents
			);
		
	  		EXIT;
	  	END IF;
		
    END LOOP;
	
   RETURN QUERY
   SELECT StudentId,
   		ProfessorId,
		PriorityId,
		AverageGrade,
	 	OwnChoice
   FROM T_Rank;
END; 

$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100
 
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- select * from GetRanks(); 

CREATE OR REPLACE FUNCTION UpdatePreferences(
	_Preferences TEXT,
	_StudentId INT,
    _AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE
  _StudentAccountId INT;
  _TypeId INT;
      BEGIN
	  	_TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
		_StudentAccountId := A.Id 
		FROM public.Account A
		INNER JOIN public.Student S ON S.AccountId = A.Id
		WHERE A.Id = _AccountId;
	  
	    IF (_TypeId = 100 OR _AccountId = _StudentAccountId) THEN
			DELETE FROM public.Preference
			WHERE StudentId = _StudentId;

			IF (_Preferences <> '' AND _Preferences <> 'null') THEN
				INSERT INTO public.Preference
				(StudentId, ProfessorId, PriorityId)
				SELECT _StudentId,
				ProfessorId,
				ROW_NUMBER () OVER () AS PriorityId
					FROM(
						SELECT CAST(regexp_split_to_table(_Preferences, ',') AS INT) AS ProfessorId
					) T;
			END IF;
			
			INSERT INTO public.Activity
			(AccountId, StudentId, TypeId)
			SELECT
			_AccountId, _StudentId, 3001;
			
		END IF;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT UpdatePreferences('', 1, 1);

CREATE OR REPLACE FUNCTION GetPreferences(
	_StudentId INT
)
  RETURNS TABLE (
	  ProfessorId INT,
	  PriorityId INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT P.ProfessorId,
			P.PriorityId
		  FROM public.Preference P
		  WHERE P.StudentId = _StudentId;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetPreferences(1);  	



CREATE OR REPLACE FUNCTION GetPreliminaryList(
	_ProfessorIdx INT,
	_PageId INT,
	_PageSize INT
)
  RETURNS TABLE (
	  RowId BIGINT,
	  RankId BIGINT,
	  StudentId INT,
	  PriorityId INT,
	  AverageGrade DECIMAL,
	  OwnChoice BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  AccountId INT,
      Username TEXT,
      TypeId INT,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  ActivityDescr TEXT,
	  ActivityDate TIMESTAMP,
	  Qualified BOOL,
	  IsRejected BOOL
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
		SELECT * 
		FROM
		(
		SELECT ROW_NUMBER () OVER (
			  ORDER BY S.Id DESC 
		   ) AS _RowId,
		   R.RankId,
			R.StudentId, 
			R.PriorityId, 
			R.AverageGrade, 
			R.OwnChoice, 
		  	S.DateCreated,
			S.DateUpdated,
		  	A.Id AS AccountId,
		  	A.Username,
		  	A.TypeId,
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN,
			LA.Descr AS ActivityDescr,
			LA.DateCreated AS ActivityDate,
			CASE WHEN (FR._professorid = _ProfessorIdx) THEN TRUE ELSE FALSE END AS Qualified,
			R.IsRejected
		FROM
		public.RankList R
		INNER JOIN public.Student S ON R.StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		LEFT JOIN public.Account A ON S.AccountId = A.Id
		LEFT JOIN public.LastStudentActivity LA ON LA.StudentId = S.Id
		LEFT JOIN public.GetRanks() FR ON FR._studentid = S.Id
		WHERE ProfessorId = _ProfessorIdx
		ORDER BY RankId
		LIMIT (_PageSize * _PageId)
	    ) T
	    WHERE _RowId > ((_PageId - 1) * _PageSize);
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetPreliminaryList(1,2,10);

CREATE OR REPLACE FUNCTION GetPreliminaryCount(
	_ProfessorId INT
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(R.RankId) AS INT)
		FROM
		public.RankList R
		INNER JOIN public.Student S ON R.StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		WHERE R.ProfessorId = _ProfessorId
		;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetPreliminaryCount(111);


CREATE OR REPLACE FUNCTION GetFinalList(
	_ProfessorIdx INT,
	_PageId INT,
	_PageSize INT
)
  RETURNS TABLE (
	  RowId BIGINT,
	  StudentId INT,
	  PriorityId INT,
	  AverageGrade DECIMAL,
	  OwnChoice BOOL,
	  DateCreated TIMESTAMP,
      DateUpdated TIMESTAMP,
	  AccountId INT,
      Username TEXT,
      TypeId INT,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT,
	  ActivityDescr TEXT,
	  ActivityDate TIMESTAMP
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
		SELECT * 
		FROM
		(
		SELECT ROW_NUMBER () OVER (
			  ORDER BY S.Id DESC 
		   ) AS _RowId,
			R._StudentId, 
			R._PriorityId, 
			R._AverageGrade, 
			R._OwnChoice, 
		  	S.DateCreated,
			S.DateUpdated,
		  	A.Id AS AccountId,
		  	A.Username,
		  	A.TypeId,
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN,
			LA.Descr AS ActivityDescr,
			LA.DateCreated AS ActivityDate
		FROM
		public.GetRanks() R
		INNER JOIN public.Student S ON R._StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		LEFT JOIN public.Account A ON S.AccountId = A.Id
		LEFT JOIN public.LastStudentActivity LA ON LA.StudentId = S.Id
		WHERE R._ProfessorId = _ProfessorIdx
		ORDER BY C.LastName
		LIMIT (_PageSize * _PageId)
	    ) T
	    WHERE _RowId > ((_PageId - 1) * _PageSize);
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetFinalList(1,1,10);

CREATE OR REPLACE FUNCTION GetFinalCount(
	_ProfessorIdx INT
)
  RETURNS TABLE (
	  Count INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
	  	SELECT CAST(COUNT(*) AS INT)
		FROM
		public.GetRanks() R
		INNER JOIN public.Student S ON R._StudentId = S.Id
		INNER JOIN public.Contact C ON S.ContactId = C.Id
		WHERE R._ProfessorId = _ProfessorIdx
		;
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetFinalCount(1);


CREATE OR REPLACE FUNCTION RejectStudent(
	_StudentId INT,
	_ProfessorId INT,
	_AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE _TypeId INT;

      BEGIN
	  
	  _TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  INSERT INTO
	  public.Reject
	  (StudentId, ProfessorId)
	  SELECT _StudentId,
	    _ProfessorId
		FROM public.Professor P
		WHERE P.Id = _ProfessorId 
		AND (_TypeId = 100 OR P.AccountId = _AccountId);
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT RejectStudent(1,1,1);


CREATE OR REPLACE FUNCTION AcceptStudent(
	_StudentId INT,
	_ProfessorId INT,
	_AccountId INT
	)
  RETURNS void AS
  $BODY$
  DECLARE _TypeId INT;

      BEGIN
	  
	  _TypeId :=  TypeId FROM public.Account WHERE Id = _AccountId;
	  
	  DELETE FROM 
	  public.Reject
	  WHERE Id IN
	  (
	  SELECT R.Id
	  FROM public.Reject R
	  INNER JOIN public.Professor P ON R.ProfessorId = P.Id
	  LEFT JOIN public.Account A ON A.Id = P.AccountId
	    WHERE R.ProfessorId = _ProfessorId 
		AND R.StudentId = _StudentId
		AND (_TypeId = 100 OR P.AccountId = _AccountId)
	  );
	  
      END  
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  
SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT AcceptStudent(1,1,1);

CREATE OR REPLACE FUNCTION GetStatus(
	_AccountId INT
)
  RETURNS TABLE (
      ProfessorId INT,
	  PriorityId INT,
	  AverageGrade DECIMAL,
	  OwnChoice BOOL,
	  Email TEXT,
      FirstName TEXT,
      LastName TEXT,
      Prefix TEXT,
      Suffix TEXT,
      JMBAG TEXT,
      Phone TEXT,
      JobDescr TEXT,
      VPN INT
  )
  AS
  $BODY$
      BEGIN
	  	RETURN QUERY
		SELECT R._ProfessorId,
			R._PriorityId, 
			R._AverageGrade, 
			R._OwnChoice, 
			C.Email,
			C.FirstName,
			C.LastName,
			C.Prefix,
			C.Suffix,
			C.JMBAG,
			C.Phone,
			C.JobDescr,
			C.VPN
		FROM
		public.GetRanks() R
		INNER JOIN public.Professor P ON R._ProfessorId = P.Id
		INNER JOIN public.Contact C ON P.ContactId = C.Id
		INNER JOIN public.Student S ON R._StudentId = S.Id
		INNER JOIN public.Account A ON S.AccountId = A.Id
		WHERE A.Id = _AccountId
		LIMIT 1;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM GetStatus(3);

CREATE OR REPLACE FUNCTION CheckAccount(
	_Email TEXT
)
  RETURNS TABLE (
	  ErrorMessage TEXT
  )
  AS
  $BODY$
  DECLARE
  _ContactExists BOOL;
  _AccountExists BOOL;
      BEGIN
	  
	  	_ContactExists := CASE WHEN EXISTS(SELECT Id FROM public.Contact WHERE Email = _Email LIMIT 1) THEN TRUE ELSE FALSE END;
		_AccountExists := CASE WHEN EXISTS(SELECT Id FROM public.Account WHERE Username = _Email LIMIT 1) THEN TRUE ELSE FALSE END;
		
		IF _ContactExists = FALSE THEN
			RETURN QUERY
			SELECT 'Profesor ili student sa ovom e-mail adresom nije pronađen.';
		
		ELSIF _AccountExists = TRUE THEN
			RETURN QUERY
			SELECT 'Korisnički račun sa ovom e-mail adresom je već registriran.';
		
		ELSE
			
			INSERT INTO
			public.Verification
			(Email, Code)
			SELECT 
			_Email, '0000'; -- random code: CAST(floor(random()*(9999-1000+1))+1000 AS TEXT);
			
			RETURN QUERY
			SELECT '';
		
		END IF;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM CheckAccount('btopic@tvz.hr');

CREATE OR REPLACE FUNCTION CreateAccount(
	_Email TEXT,
	_Password TEXT
)
  RETURNS TABLE (
	  ErrorMessage TEXT
  )
  AS
  $BODY$
  DECLARE
  _AccountExists BOOL;
  _TypeId INT;
  _ContactId INT;
  _AccountId INT;
      BEGIN
	  
	  	_AccountExists := CASE WHEN EXISTS(SELECT Id FROM public.Account WHERE Username = _Email LIMIT 1) THEN TRUE ELSE FALSE END;
	  	    SELECT CASE WHEN (S.Id IS NOT NULL) THEN 300 WHEN (P.Id IS NOT NULL) THEN 200 END,
		    C.Id
			FROM
			public.Contact C
			LEFT JOIN public.Student S ON S.ContactId = C.Id
			LEFT JOIN public.Professor P ON P.ContactId = C.Id
			WHERE C.Email = _Email
			INTO _TypeId, _ContactId ;
		
		IF _AccountExists = TRUE THEN
			RETURN QUERY
			SELECT 'Korisnički račun sa ovom e-mail adresom je već registriran.';
		ELSE
	  
			 INSERT INTO public.Account
			(Username, Password, ContactId, TypeId)
			SELECT C.Email,
				_Password,
				_ContactId,
				_TypeId
			FROM
			public.Contact C
			LEFT JOIN public.Student S ON S.ContactId = C.Id
			LEFT JOIN public.Professor P ON P.ContactId = C.Id
			WHERE C.Email = _Email
			RETURNING id INTO _AccountId;
			
			IF _TypeId = 200 THEN
				UPDATE public.Professor SET
				AccountId = _AccountId,
				DateUpdated = CURRENT_TIMESTAMP
				WHERE ContactId = _ContactId;
			ELSIF _TypeId = 300 THEN
				UPDATE public.Student SET
				AccountId = _AccountId,
				DateUpdated = CURRENT_TIMESTAMP
				WHERE ContactId = _ContactId;
			END IF;
			
			RETURN QUERY
			SELECT '';
		
		END IF;

      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM CreateAccount('btopic@tvz.hr', 'encryptedpassword');

CREATE OR REPLACE FUNCTION Verify(
	_Email TEXT,
	_Code TEXT
)
  RETURNS TABLE (
	  ErrorMessage TEXT
  )
  AS
  $BODY$
  DECLARE
  _SavedCode TEXT;
  _AccountExists BOOL;
      BEGIN
	  
	  	_SavedCode := Code FROM public.Verification WHERE Email = _Email ORDER BY DateCreated DESC LIMIT 1;
		
		IF _Code <> _SavedCode THEN
			RETURN QUERY
			SELECT 'Kod je netočan ili zastarjeo.';
			
		ELSE
			RETURN QUERY
			SELECT '';
		
		END IF;
		
      END
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100

SECURITY DEFINER
SET search_path = webusers, pg_temp;

-- SELECT * FROM Verify('btopic@tvz.hr', '6527');

-- CREATE ADMIN ACCOUNT ---------------
INSERT INTO Contact (Email, FirstName, LastName)
SELECT 'admin@admin.com', 'Admin', 'Test';

INSERT INTO Account (Username, Password, TypeId, ContactId)
SELECT 'admin@admin.com', '$2b$12$lyOKfPHfczfgznz7uL0IoOTlK/FG9S0cIzHyMj.O.kWQjbrUSA2c6', 100, 1;
-- bcrypt: "sifra123"
--select * from Account;

-- CREATE PROFESSOR ACCOUNT -----------------
INSERT INTO Contact (Email, FirstName, LastName, Prefix, Suffix)
SELECT 'ikolar@tvz.hr', 'Ivan', 'Kolar', 'dr. sc.', 'viši predavač';

INSERT INTO Account (Username, Password, TypeId, ContactId)
SELECT 'ikolar@tvz.hr', '$2b$12$lyOKfPHfczfgznz7uL0IoOTlK/FG9S0cIzHyMj.O.kWQjbrUSA2c6', 200, 2;
-- bcrypt: "sifra123"
--select * from Account;

INSERT INTO Professor (ContactId, AccountId)
SELECT 2,2;

-- CREATE STUDENT ACCOUNT -----------------
INSERT INTO Contact (Email, FirstName, LastName, JMBAG)
SELECT 'lnovak@tvz.hr', 'Luka', 'Novak', '0249080843';

INSERT INTO Account (Username, Password, TypeId, ContactId)
SELECT 'lnovak@tvz.hr', '$2b$12$lyOKfPHfczfgznz7uL0IoOTlK/FG9S0cIzHyMj.O.kWQjbrUSA2c6', 300, 3;
-- bcrypt: "sifra123"
--select * from Account;

INSERT INTO Student (ContactId, AccountId)
SELECT 3,3;

-------------------- GENERATE DATA -------------------------
INSERT INTO public.Config (MaxStudents) 
SELECT 10;

INSERT INTO Name (Descr)
SELECT 'Mia'
UNION ALL
SELECT 'Sara'
UNION ALL
SELECT 'Lucija'
UNION ALL
SELECT 'Ema'
UNION ALL
SELECT 'Ana'
UNION ALL
SELECT 'Nika'
UNION ALL
SELECT 'Petra'
UNION ALL
SELECT 'Marta'
UNION ALL
SELECT 'Lana'
UNION ALL
SELECT 'Rita'
UNION ALL
SELECT 'Elena'
UNION ALL
SELECT 'Iva'
UNION ALL
SELECT 'Lea'
UNION ALL
SELECT 'Eva'
UNION ALL
SELECT 'Lara'
UNION ALL
SELECT 'Marija'
UNION ALL
SELECT 'Dora'
UNION ALL
SELECT 'Laura'
UNION ALL
SELECT 'Klara'
UNION ALL
SELECT 'Mila'
UNION ALL
SELECT 'Leona'
UNION ALL
SELECT 'Tena'
UNION ALL
SELECT 'Ena'
UNION ALL
SELECT 'Hana'
UNION ALL
SELECT 'Franka'
UNION ALL
SELECT 'Lena'
UNION ALL
SELECT 'Karla'
UNION ALL
SELECT 'Katja'
UNION ALL
SELECT 'Maša'
UNION ALL
SELECT 'Lorena'
UNION ALL
SELECT 'Magdalena'
UNION ALL
SELECT 'Katarina'
UNION ALL
SELECT 'Tia'
UNION ALL
SELECT 'Tara'
UNION ALL
SELECT 'Helena'
UNION ALL
SELECT 'Vita'
UNION ALL
SELECT 'Sofia'
UNION ALL
SELECT 'Nora'
UNION ALL
SELECT 'Tea'
UNION ALL
SELECT 'Nikol'
UNION ALL
SELECT 'Nina'
UNION ALL
SELECT 'Una'
UNION ALL
SELECT 'Ella'
UNION ALL
SELECT 'Ela'
UNION ALL
SELECT 'Bruna'
UNION ALL
SELECT 'Matea'
UNION ALL
SELECT 'Paula'
UNION ALL
SELECT 'Doris'
UNION ALL
SELECT 'Gabriela'
UNION ALL
SELECT 'Jana'
UNION ALL
SELECT 'Luka'
UNION ALL
SELECT 'David'
UNION ALL
SELECT 'Ivan'
UNION ALL
SELECT 'Jakov'
UNION ALL
SELECT 'Petar'
UNION ALL
SELECT 'Filip'
UNION ALL
SELECT 'Marko'
UNION ALL
SELECT 'Matej'
UNION ALL
SELECT 'Mihael'
UNION ALL
SELECT 'Mateo'
UNION ALL
SELECT 'Leon'
UNION ALL
SELECT 'Josip'
UNION ALL
SELECT 'Karlo'
UNION ALL
SELECT 'Ivano'
UNION ALL
SELECT 'Fran'
UNION ALL
SELECT 'Noa'
UNION ALL
SELECT 'Roko'
UNION ALL
SELECT 'Borna'
UNION ALL
SELECT 'Jan'
UNION ALL
SELECT 'Niko'
UNION ALL
SELECT 'Gabriel'
UNION ALL
SELECT 'Ante'
UNION ALL
SELECT 'Lovro'
UNION ALL
SELECT 'Toma'
UNION ALL
SELECT 'Lukas'
UNION ALL
SELECT 'Vito'
UNION ALL
SELECT 'Nikola'
UNION ALL
SELECT 'Teo'
UNION ALL
SELECT 'Leo'
UNION ALL
SELECT 'Dominik'
UNION ALL
SELECT 'Marin'
UNION ALL
SELECT 'Patrik'
UNION ALL
SELECT 'Šimun'
UNION ALL
SELECT 'Tin'
UNION ALL
SELECT 'Toni'
UNION ALL
SELECT 'Matija'
UNION ALL
SELECT 'Antonio'
UNION ALL
SELECT 'Emanuel'
UNION ALL
SELECT 'Bruno'
UNION ALL
SELECT 'Andrej'
UNION ALL
SELECT 'Viktor'
UNION ALL
SELECT 'Erik'
UNION ALL
SELECT 'Adrian'
UNION ALL
SELECT 'Noel'
UNION ALL
SELECT 'Rafael'
UNION ALL
SELECT 'Duje'
UNION ALL
SELECT 'Gabrijel'
UNION ALL
SELECT 'Lovre'
UNION ALL
SELECT 'Martin'
UNION ALL
SELECT 'Dino';

INSERT INTO Surname (Descr)
SELECT 'Babić'
UNION ALL
SELECT 'Barić'
UNION ALL
SELECT 'Barišić'
UNION ALL
SELECT 'Bačić'
UNION ALL
SELECT 'Bašić'
UNION ALL
SELECT 'Bilić'
UNION ALL
SELECT 'Blažević'
UNION ALL
SELECT 'Bogdan'
UNION ALL
SELECT 'Bošnjak'
UNION ALL
SELECT 'Božić'
UNION ALL
SELECT 'Brajković'
UNION ALL
SELECT 'Brkić'
UNION ALL
SELECT 'Burić'
UNION ALL
SELECT 'Cindrić'
UNION ALL
SELECT 'Delić'
UNION ALL
SELECT 'Dujmović'
UNION ALL
SELECT 'Filipović'
UNION ALL
SELECT 'Galić'
UNION ALL
SELECT 'Grgić'
UNION ALL
SELECT 'Grubišić'
UNION ALL
SELECT 'Herceg'
UNION ALL
SELECT 'Horvat'
UNION ALL
SELECT 'Ilić'
UNION ALL
SELECT 'Ivanković'
UNION ALL
SELECT 'Ivanović'
UNION ALL
SELECT 'Ivančić'
UNION ALL
SELECT 'Jakšić'
UNION ALL
SELECT 'Janković'
UNION ALL
SELECT 'Jelić'
UNION ALL
SELECT 'Jerković'
UNION ALL
SELECT 'Jovanović'
UNION ALL
SELECT 'Jozić'
UNION ALL
SELECT 'Jukić'
UNION ALL
SELECT 'Jurić'
UNION ALL
SELECT 'Jurišić'
UNION ALL
SELECT 'Jurković'
UNION ALL
SELECT 'Katić'
UNION ALL
SELECT 'Klarić'
UNION ALL
SELECT 'Knežević'
UNION ALL
SELECT 'Kolar'
UNION ALL
SELECT 'Kos'
UNION ALL
SELECT 'Kovač'
UNION ALL
SELECT 'Kovačević'
UNION ALL
SELECT 'Kovačić'
UNION ALL
SELECT 'Kralj'
UNION ALL
SELECT 'Lončar'
UNION ALL
SELECT 'Lovrić'
UNION ALL
SELECT 'Lukić'
UNION ALL
SELECT 'Lučić'
UNION ALL
SELECT 'Mandić'
UNION ALL
SELECT 'Marinović'
UNION ALL
SELECT 'Marić'
UNION ALL
SELECT 'Marjanović'
UNION ALL
SELECT 'Marković'
UNION ALL
SELECT 'Martinović'
UNION ALL
SELECT 'Marušić'
UNION ALL
SELECT 'Matijević'
UNION ALL
SELECT 'Matić'
UNION ALL
SELECT 'Matković'
UNION ALL
SELECT 'Mihaljević'
UNION ALL
SELECT 'Mikulić'
UNION ALL
SELECT 'Miletić'
UNION ALL
SELECT 'Milić'
UNION ALL
SELECT 'Nikolić'
UNION ALL
SELECT 'Novak'
UNION ALL
SELECT 'Novaković'
UNION ALL
SELECT 'Novosel'
UNION ALL
SELECT 'Pavić'
UNION ALL
SELECT 'Pavičić'
UNION ALL
SELECT 'Pavlović'
UNION ALL
SELECT 'Pejić'
UNION ALL
SELECT 'Perić'
UNION ALL
SELECT 'Perković'
UNION ALL
SELECT 'Petković'
UNION ALL
SELECT 'Petrić'
UNION ALL
SELECT 'Petrović'
UNION ALL
SELECT 'Poljak'
UNION ALL
SELECT 'Popović'
UNION ALL
SELECT 'Posavec'
UNION ALL
SELECT 'Pranjić'
UNION ALL
SELECT 'Radić'
UNION ALL
SELECT 'Rukavina'
UNION ALL
SELECT 'Stanić'
UNION ALL
SELECT 'Tadić'
UNION ALL
SELECT 'Tomić'
UNION ALL
SELECT 'Topić'
UNION ALL
SELECT 'Varga'
UNION ALL
SELECT 'Vidaković'
UNION ALL
SELECT 'Vidović'
UNION ALL
SELECT 'Vukelić'
UNION ALL
SELECT 'Vuković'
UNION ALL
SELECT 'Vučković'
UNION ALL
SELECT 'Ćosić'
UNION ALL
SELECT 'Đurić'
UNION ALL
SELECT 'Šarić'
UNION ALL
SELECT 'Šimić'
UNION ALL
SELECT 'Šimunović'
UNION ALL
SELECT 'Špoljarić'
UNION ALL
SELECT 'Štimac'
UNION ALL
SELECT 'Živković';

INSERT INTO Course
(Descr)
SELECT '3D modeliranje'
UNION ALL
SELECT 'Administriranje i poslovanje mrežama računala'
UNION ALL
SELECT 'Administriranje UNIX sustava'
UNION ALL
SELECT 'Baze podataka'
UNION ALL
SELECT 'Digitalna animacija'
UNION ALL
SELECT 'Digitalna fotografija'
UNION ALL
SELECT 'Digitalna televizija'
UNION ALL
SELECT 'Dizajn i primjena vektorske grafike'
UNION ALL
SELECT 'Dizajn i vizualno značenje'
UNION ALL
SELECT 'Dizajn proizvoda'
UNION ALL
SELECT 'Dizajn vizualnih komunikacija'
UNION ALL
SELECT 'Društvene mreže'
UNION ALL
SELECT 'Elektroničko poslovanje'
UNION ALL
SELECT 'Engleski jezik za IT'
UNION ALL
SELECT 'Fizika'
UNION ALL
SELECT 'Građa računala'
UNION ALL
SELECT 'Grafičke tehnike'
UNION ALL
SELECT 'Grafički dizajn'
UNION ALL
SELECT 'Grafički programski jezici'
UNION ALL
SELECT 'Informacijska pismenost i kritičko razmišljanje'
UNION ALL
SELECT 'Inovacije u informatici'
UNION ALL
SELECT 'Instalacija računala i programa'
UNION ALL
SELECT 'Integracija medija'
UNION ALL
SELECT 'Interaktivno programiranje na Web-u'
UNION ALL
SELECT 'Interaktivno programiranje na Web-u'
UNION ALL
SELECT 'Kineziološka kultura I'
UNION ALL
SELECT 'Kineziološka kultura II'
UNION ALL
SELECT 'Kineziološka kultura III'
UNION ALL
SELECT 'Kineziološka kultura IV'
UNION ALL
SELECT 'Komunikacijski sustavi i mreže'
UNION ALL
SELECT 'Matematika I'
UNION ALL
SELECT 'Matematika II'
UNION ALL
SELECT 'Mobilne komunikacije'
UNION ALL
SELECT 'Multimedijski marketing'
UNION ALL
SELECT 'Napredne baze podataka'
UNION ALL
SELECT 'Napredne tehnologije interneta'
UNION ALL
SELECT 'Napredno elektroničko poslovanje'
UNION ALL
SELECT 'Njemački jezik za IT'
UNION ALL
SELECT 'Objektno orijentirano programiranje I'
UNION ALL
SELECT 'Objektno orijentirano programiranje II'
UNION ALL
SELECT 'Oblikovanje e literature'
UNION ALL
SELECT 'Oblikovanje Web stranica'
UNION ALL
SELECT 'Obrada slike, zvuka i videa'
UNION ALL
SELECT 'Obrada slike, zvuka i videa'
UNION ALL
SELECT 'Obrada teksta'
UNION ALL
SELECT 'Operacijski sustavi'
UNION ALL
SELECT 'Organizacija i informatizacija ureda'
UNION ALL
SELECT 'Osnove programiranja'
UNION ALL
SELECT 'Poslovni engleski jezik za IT'
UNION ALL
SELECT 'Poslovni njemački jezik za IT'
UNION ALL
SELECT 'Praktikum iz dizajna'
UNION ALL
SELECT 'Pretražnici i navigacija na Web-u'
UNION ALL
SELECT 'Procesi video produkcije'
UNION ALL
SELECT 'Produkcija zvuka'
UNION ALL
SELECT 'Programiranje'
UNION ALL
SELECT 'Programiranje web aplikacija'
UNION ALL
SELECT 'Projektno programiranje'
UNION ALL
SELECT 'Računalna grafika'
UNION ALL
SELECT 'Računalna tipografija'
UNION ALL
SELECT 'Razvoj računalnih igara'
UNION ALL
SELECT 'Reprofotografija'
UNION ALL
SELECT 'Sigurnost i zaštita informacijskih sustava'
UNION ALL
SELECT 'Socio tehnološki aspekti Informacijskih sustava'
UNION ALL
SELECT 'Stručna praksa'
UNION ALL
SELECT 'Sustavi elektroničkog poslovanja'
UNION ALL
SELECT 'Tablični kalkulatori'
UNION ALL
SELECT 'Tehnološko poduzetništvo'
UNION ALL
SELECT 'Tehnološko poduzetništvo'
UNION ALL
SELECT 'Teorija i razvoj dizajna'
UNION ALL
SELECT 'Tržišne komunikacije'
UNION ALL
SELECT 'TV i Video snimanje'
UNION ALL
SELECT 'Uredsko poslovanje'
UNION ALL
SELECT 'Uvod u (X)HTML i CSS'
UNION ALL
SELECT 'Uvod u mreže računala'
UNION ALL
SELECT 'Uvod u UNIX sustave'
UNION ALL
SELECT 'Vjerojatnost i statistika'
UNION ALL
SELECT 'Vještine komuniciranja'
UNION ALL
SELECT 'XML programiranje';


INSERT INTO AccountType (Id, Descr)
SELECT 100, 'Admin'
UNION ALL
SELECT 200, 'Professor'
UNION ALL
SELECT 300, 'Student'
;
--select * from AccountType;

INSERT INTO ActivityType (Id, Descr)
SELECT 2001, 'Odbijanje ili prihvaćanje studenta'
UNION ALL
SELECT 3001, 'Izmjena preference mentora'
;

-- generate students and professors
SELECT * FROM Regenerate(100,10,TRUE,1);

