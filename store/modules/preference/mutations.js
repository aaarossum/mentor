import * as types from './mutation-types'

export default {
  [types.SET_PREFERENCES](state, data) {
    state.preferences = data
  },
  [types.UPDATE_PREFERENCES](state) {}
}
