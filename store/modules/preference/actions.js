import axios from 'axios'
import * as mutations from './mutation-types'

export const fetchAll = ({ commit }, studentId) => {
  axios
    .get(`/api/preferences/` + studentId)
    .then((json) => {
      commit(mutations.SET_PREFERENCES, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const update = ({ commit }, data) => {
  return axios
    .post(
      `/api/preferences/update/` +
        toDelimited(data.preferences) +
        `&` +
        data.studentId
    )
    .then((json) => {
      commit(mutations.UPDATE_PREFERENCES)
      return json
    })
    .catch((error) => {
      return error
    })
}

const toDelimited = function(array) {
  let output = ''
  for (let i = 0; i < array.length; i++) {
    output += array[i].id
    output += i !== array.length - 1 ? ',' : ''
  }
  output = output === '' ? 'null' : output
  return output
}
