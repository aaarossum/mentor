import * as types from './mutation-types'

export default {
  [types.SET_PROFESSORS](state, data) {
    state.professors = data
  },
  [types.SET_COUNT](state, data) {
    state.professorCount = data.count
  },
  [types.RESET_PROFESSORS](state) {
    state.professors = []
    state.professorCount = 0
  },
  [types.SET_STATUS](state, data) {
    state.status = data
  }
}
