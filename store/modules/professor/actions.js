import axios from 'axios'
import * as mutations from './mutation-types'

export const fetchAll = ({ commit }, pagination) => {
  axios
    .get(`/api/professors/` + pagination.current + '&' + pagination.pageSize)
    .then((json) => {
      commit(mutations.SET_PROFESSORS, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const count = ({ commit }) => {
  axios
    .get(`/api/professors/count/`)
    .then((json) => {
      commit(mutations.SET_COUNT, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const fetchList = ({ commit }) => {
  axios
    .get(`/api/professors/all/`)
    .then((json) => {
      commit(mutations.SET_PROFESSORS, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const status = ({ commit }, data) => {
  axios
    .get(`/api/professors/status/`)
    .then((json) => {
      commit(mutations.SET_STATUS, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const reset = ({ commit }) => {
  commit(mutations.RESET_PROFESSORS)
}
