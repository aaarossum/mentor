export const professors = (store) => store.professors
export const professorCount = (store) => store.professorCount
export const status = (store) => store.status
