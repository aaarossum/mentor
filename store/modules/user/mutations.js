import * as types from './mutation-types'

export default {
  [types.SET_USER](state, data) {
    state.user = data
  },
  [types.RESET_USER](state) {
    state.user = null
  }
}
