import axios from 'axios'
import cookies from 'js-cookie'
import * as mutations from './mutation-types'
import { setAuthToken, resetAuthToken } from '~/utils/auth'

export const fetch = ({ commit }) => {
  return axios
    .get(`/api/me`)
    .then((response) => {
      commit(mutations.SET_USER, response.data.user)
      return response
    })
    .catch((error) => {
      commit(mutations.RESET_USER)
      return error
    })
}

export const login = ({ commit }, data) => {
  return axios.post(`/api/login`, data).then((response) => {
    commit(mutations.SET_USER, response.data.user)
    setAuthToken(response.data.token)
    cookies.set('x-access-token', response.data.token, { expires: 7 })
    return response
  })
}

export const reset = ({ commit }) => {
  commit(mutations.RESET_USER)
  resetAuthToken()
  cookies.remove('x-access-token')
  return Promise.resolve()
}

export const checkRegistration = ({ commit }, Email) => {
  return axios.post(`/api/user/checkaccount/` + Email).then((response) => {
    return response
  })
}

export const verify = ({ commit }, data) => {
  return axios
    .post(`/api/user/verify/` + data.Email + '&' + data.Code)
    .then((response) => {
      return response
    })
}

export const create = ({ commit }, data) => {
  return axios
    .post(`/api/user/create/` + data.Email + '&' + data.Password)
    .then((response) => {
      commit(mutations.SET_USER, response.data.user)
      setAuthToken(response.data.token)
      cookies.set('x-access-token', response.data.token, { expires: 7 })
      return response
    })
}
