import * as types from './mutation-types'

export default {
  [types.SET_CONFIG](state, data) {
    state.config = data
  }
}
