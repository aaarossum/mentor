import axios from 'axios'
import * as mutations from './mutation-types'

export const fetch = ({ commit }) => {
  axios
    .get(`/api/config/`)
    .then((json) => {
      commit(mutations.SET_CONFIG, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const save = ({ commit }, data) => {
  return axios
    .post(`/api/config/save/` + data.maxStudents)
    .then((json) => {
      return json
    })
    .catch((error) => {
      return error
    })
}

export const regenerate = ({ commit }, data) => {
  return axios
    .post(
      `/api/regenerate/` +
        data.studentCount +
        `&` +
        data.professorCount +
        `&` +
        data.preferences
    )
    .then((json) => {
      return json
    })
    .catch((error) => {
      return error
    })
}
