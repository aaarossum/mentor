import axios from 'axios'
import * as mutations from './mutation-types'

export const fetchAll = ({ commit }, data) => {
  axios
    .get(
      `/api/students/` +
        data.pagination.current +
        '&' +
        data.pagination.pageSize +
        '&' +
        data.professorId
    )
    .then((json) => {
      commit(mutations.SET_STUDENTS, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const count = ({ commit }) => {
  axios
    .get(`/api/students/count/`)
    .then((json) => {
      commit(mutations.SET_COUNT, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const fetchPreliminary = ({ commit }, data) => {
  axios
    .get(
      `/api/students/preliminary/` +
        data.professorId +
        '&' +
        data.pagination.current +
        '&' +
        data.pagination.pageSize
    )
    .then((json) => {
      commit(mutations.SET_STUDENTS, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const preliminaryCount = ({ commit }, professorId) => {
  axios
    .get(`/api/students/preliminary/count/` + professorId)
    .then((json) => {
      commit(mutations.SET_COUNT, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const fetchFinal = ({ commit }, data) => {
  axios
    .get(
      `/api/students/final/` +
        data.professorId +
        '&' +
        data.pagination.current +
        '&' +
        data.pagination.pageSize
    )
    .then((json) => {
      commit(mutations.SET_STUDENTS, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const finalCount = ({ commit }, professorId) => {
  axios
    .get(`/api/students/final/count/` + professorId)
    .then((json) => {
      commit(mutations.SET_COUNT, json.data)
    })
    .catch((error) => {
      return error
    })
}

export const reject = ({ commit }, data) => {
  return axios
    .post(`/api/student/reject/` + data.studentId + `&` + data.professorId)
    .then((json) => {
      return json
    })
    .catch((error) => {
      return error
    })
}

export const accept = ({ commit }, data) => {
  return axios
    .post(`/api/student/accept/` + data.studentId + `&` + data.professorId)
    .then((json) => {
      return json
    })
    .catch((error) => {
      return error
    })
}

export const reset = ({ commit }) => {
  commit(mutations.RESET_STUDENTS)
}
