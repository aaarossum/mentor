import * as types from './mutation-types'

export default {
  [types.SET_STUDENTS](state, data) {
    state.students = data
  },
  [types.SET_COUNT](state, data) {
    state.studentCount = data.count
  },
  [types.RESET_STUDENTS](state) {
    state.students = []
    state.studentCount = 0
  }
}
