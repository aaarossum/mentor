import Vuex from 'vuex'
import user from './modules/user'
import student from './modules/student'
import professor from './modules/professor'
import config from './modules/config'
import preference from './modules/preference'

const createStore = () =>
  new Vuex.Store({
    modules: {
      user,
      student,
      professor,
      config,
      preference
    }
  })
export default createStore
