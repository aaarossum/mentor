// const auth = require('./auth')

import express from 'express'
require('dotenv').config()

const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const app = express()

const defaultErrorMessage = 'Došlo je do pogreške.'

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

const Pool = require('pg').Pool
const pool = new Pool({
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  password: process.env.PGPASSWORD,
  port: process.env.PGPORT
})

const getItems = (req, res) => {
  getAllItems(req, res)
}

const getAllItems = function(req, res) {
  pool.query('SELECT * FROM Item;', (error, results) => {
    if (error) {
      res.status(403).json({ type: 'error', message: defaultErrorMessage })
    } else res.status(200).json(results.rows)
  })
}

app.get('/items', getItems)

app.get('/students/count/', async (req, res) => {
  await pool.query(`SELECT * FROM GetStudentCount()`, (error, results) => {
    if (error) {
      res.status(403).json({ type: 'error', message: defaultErrorMessage })
    } else res.status(200).json(results.rows[0])
  })
})

app.get(
  '/students/:pageid&:pagesize&:professorid',
  async ({ params: { pageid, pagesize, professorid } }, res) => {
    await pool.query(
      `SELECT * FROM GetStudents(` +
        pageid +
        `,` +
        pagesize +
        `,` +
        professorid +
        `)`,
      (error, results) => {
        const output = []
        for (let index = 0; index < results.rows.length; ++index) {
          const row = results.rows[index]
          row.professor = {
            firstname: results.rows[index].professorfirstname,
            lastname: results.rows[index].professorlastname,
            prefix: results.rows[index].professorprefix,
            suffix: results.rows[index].professorsuffix
          }
          output.push(row)
        }
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        } else res.status(200).json(output)
      }
    )
  }
)

app.get(
  '/students/preliminary/count/:professorId',
  async ({ params: { professorId } }, res) => {
    await pool.query(
      `SELECT * FROM GetPreliminaryCount(` + professorId + `)`,
      (error, results) => {
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        }
        res.status(200).json(results.rows[0])
      }
    )
  }
)

app.get(
  '/students/preliminary/:professorId&:pageid&:pagesize',
  async ({ params: { professorId, pageid, pagesize } }, res) => {
    await pool.query(
      `SELECT * FROM GetPreliminaryList(` +
        professorId +
        `,` +
        pageid +
        `,` +
        pagesize +
        `)`,
      (error, results) => {
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        } else res.status(200).json(results.rows)
      }
    )
  }
)

app.get(
  '/students/final/count/:professorId',
  async ({ params: { professorId } }, res) => {
    await pool.query(
      `SELECT * FROM GetFinalCount(` + professorId + `)`,
      (error, results) => {
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        } else res.status(200).json(results.rows[0])
      }
    )
  }
)

app.get(
  '/students/final/:professorId&:pageid&:pagesize',
  async ({ params: { professorId, pageid, pagesize } }, res) => {
    await pool.query(
      `SELECT * FROM GetFinalList(` +
        professorId +
        `,` +
        pageid +
        `,` +
        pagesize +
        `)`,
      (error, results) => {
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        } else res.status(200).json(results.rows)
      }
    )
  }
)

app.post('/student/reject/:studentid&:professorid', async (req, res) => {
  const user = await auth(req)
  await pool.query(
    `SELECT RejectStudent(` +
      req.params.studentid +
      `,` +
      req.params.professorid +
      `,` +
      user.id +
      `)`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else res.status(200).json('ok')
    }
  )
})

app.post('/student/accept/:studentid&:professorid', async (req, res) => {
  const user = await auth(req)
  await pool.query(
    `SELECT AcceptStudent(` +
      req.params.studentid +
      `,` +
      req.params.professorid +
      `,` +
      user.id +
      `)`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else res.status(200).json('ok')
    }
  )
})

app.get('/professors/count/', async (req, res) => {
  await pool.query(`SELECT * FROM GetProfessorCount()`, (error, results) => {
    if (error) {
      res.status(403).json({ type: 'error', message: defaultErrorMessage })
    } else res.status(200).json(results.rows[0])
  })
})

app.get(
  '/professors/:pageid&:pagesize',
  async ({ params: { pageid, pagesize } }, res) => {
    await pool.query(
      `SELECT * FROM GetProfessors(` + pageid + `,` + pagesize + `)`,
      (error, results) => {
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        } else res.status(200).json(results.rows)
      }
    )
  }
)

app.get('/professors/all/', async (req, res) => {
  await pool.query(`SELECT * FROM GetProfessorsAll()`, (error, results) => {
    if (error) {
      res.status(403).json({ type: 'error', message: defaultErrorMessage })
    } else res.status(200).json(results.rows)
  })
})

app.get('/professors/status/', async (req, res) => {
  const user = await auth(req)
  await pool.query(
    `SELECT * FROM GetStatus(` + user.id + `)`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else res.status(200).json(results.rows[0])
    }
  )
})

app.get('/config/', async (req, res) => {
  await pool.query(`SELECT * FROM GetConfig()`, (error, results) => {
    if (error) {
      res.status(403).json({ type: 'error', message: defaultErrorMessage })
    } else res.status(200).json(results.rows[0])
  })
})

const auth = async (req) => {
  const token = req.headers['x-access-token']
  if (!token) return 0
  const resultToken = await jwt.verify(token, process.env.JWTTOKEN)
  const resultSQL = await pool.query(
    `SELECT * FROM GetAccount(` + resultToken.id + `)`
  )
  return resultSQL.rows[0]
}

app.post('/user/checkaccount/:email', async (req, res) => {
  await pool.query(
    `SELECT * FROM CheckAccount('` + req.params.email + `')`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else if (results.rows[0].errormessage) {
        res
          .status(403)
          .json({ type: 'error', message: results.rows[0].errormessage })
      } else res.status(200).json('ok')
    }
  )
})

app.post('/user/verify/:email&:code', async (req, res) => {
  await pool.query(
    `SELECT * FROM Verify('` +
      req.params.email +
      `','` +
      req.params.code +
      `')`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else if (results.rows[0].errormessage) {
        res
          .status(403)
          .json({ type: 'error', message: results.rows[0].errormessage })
      } else res.status(200).json('ok')
    }
  )
})

app.post('/user/create/:email&:password', async (req, res) => {
  const hash = await bcrypt.hash(req.params.password, 10)
  await pool.query(
    `SELECT * FROM CreateAccount('` + req.params.email + `','` + hash + `')`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else if (results.rows[0].errormessage) {
        res
          .status(403)
          .json({ type: 'error', message: results.rows[0].errormessage })
      } else {
        pool.query(
          `select id, username, password, typeid from account where username ='` +
            req.params.email +
            `'`,
          (error, results) => {
            if (error)
              return res.status(500).json({
                type: 'error',
                message: 'Pogreška sa bazom podataka',
                error
              })
            if (results.rows.length === 0)
              return res.status(403).json({
                type: 'error',
                message: 'Korisničko ime nije pronađeno.'
              })
            const user = results.rows[0]
            res.status(200).json({
              type: 'success',
              message: 'Korsinik logiran.',
              user: {
                id: user.id,
                username: user.username,
                typeId: user.typeid
              },
              token: jwt.sign(
                { id: user.id, username: user.username },
                process.env.JWTTOKEN,
                { expiresIn: '7d' }
              )
            })
          }
        )
      }
    }
  )
})

app.post('/config/save/:maxstudents', async (req, res) => {
  const user = await auth(req)
  await pool.query(
    `SELECT SaveConfig(` + req.params.maxstudents + `,` + user.id + `)`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else res.status(200).json('ok')
    }
  )
})

app.post(
  '/regenerate/:studentcount&:professorcount&:preferences',
  async (req, res) => {
    const user = await auth(req)
    await pool.query(
      `SELECT Regenerate(` +
        req.params.studentcount +
        `,` +
        req.params.professorcount +
        `,` +
        req.params.preferences +
        `,` +
        user.id +
        `)`,
      (error, results) => {
        if (error) {
          res.status(403).json({ type: 'error', message: defaultErrorMessage })
        } else res.status(200).json('ok')
      }
    )
  }
)

app.get('/preferences/:studentid', async ({ params: { studentid } }, res) => {
  await pool.query(
    `SELECT * FROM GetPreferences(` + studentid + `)`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else res.status(200).json(results.rows)
    }
  )
})

app.post('/preferences/update/:preferences&:studentid', async (req, res) => {
  const user = await auth(req)
  await pool.query(
    `SELECT UpdatePreferences('` +
      req.params.preferences +
      `',` +
      req.params.studentid +
      `,` +
      user.id +
      `)`,
    (error, results) => {
      if (error) {
        res.status(403).json({ type: 'error', message: defaultErrorMessage })
      } else res.status(200).json('ok')
    }
  )
})

app.post('/login', (req, res) => {
  const username = req.body.username
  const password = req.body.password
  if (!username || !password)
    return res.status(400).json({
      type: 'error',
      message: 'Korisničko ime i lozinka su obavezna polja.'
    })
  pool.query(
    `select id, username, password, typeid from account where username='` +
      username +
      `'`,
    (error, results) => {
      if (error)
        return res.status(500).json({
          type: 'error',
          message: 'Pogreška sa bazom podataka.',
          error
        })
      if (results.rows.length === 0)
        return res
          .status(403)
          .json({ type: 'error', message: 'Korisničko ime nije pronađeno.' })
      const user = results.rows[0]
      bcrypt.compare(password, user.password, (error, result) => {
        if (error)
          return res
            .status(500)
            .json({ type: 'error', message: 'bcrypt error', error })
        else if (result) {
          return res.json({
            type: 'success',
            message: 'Korsinik logiran.',
            user: { id: user.id, username: user.username, typeId: user.typeid },
            token: jwt.sign(
              { id: user.id, username: user.username },
              process.env.JWTTOKEN,
              { expiresIn: '7d' }
            )
          })
        } else
          return res.status(403).json({
            type: 'error',
            message: 'Korisničko ime ili lozinka je neispravna.'
          })
      })
    }
  )
})

app.get('/me', (req, res) => {
  const token = req.headers['x-access-token']
  if (!token)
    return res
      .status(400)
      .json({ type: 'error', message: 'x-access-token header not found.' })
  jwt.verify(token, process.env.JWTTOKEN, (error, result) => {
    if (error)
      return res.status(403).json({
        type: 'error',
        message:
          'Token je nevažeći. Molimo očistite kolačiće ili otvorite privatnu sesiju.',
        error
      })
    else {
      pool.query(
        `SELECT * FROM GetAccount(` + result.id + `)`,
        (error, results) => {
          const user = results.rows[0]
          if (error) {
            res
              .status(403)
              .json({ type: 'error', message: defaultErrorMessage })
          } else
            return res.json({
              type: 'success',
              message: 'Token je valjan.',
              result,
              user
            })
        }
      )
    }
  })
})

module.exports = {
  path: '/api/',
  handler: app
}
