# Mentor

SQL database (postgreqsql) :

* create new account `webusers` 
* execute joined script  `SQL/joined.sql` that includes all SQL scripts

The script creates all needed tables and generates mockup data.

Mentor front-end project (NuxtJS):

* edit the `.env`: password for postgres account `webusers` under `PGPASSWORD`
* Node.js install: `npm install`
* Development mode: `npm run dev`
* Build: `npm run build`
* Start: `npm run start`
* Linting the code: `npm run lintfix`

Note: The project uses my own fork of ant-design-vue with Croatian localization.
Support of the localization is expected in 1.5.0.

Starting login details for admin in database:

​	Username: `admin@admin.com`

​	Password: `sifra123`

To do for a production version:

* security of XHR calls (base64 of passwords, sanitation, router, origin etc.)
* implement sending e-mail inside `checkaccount` method
* implement validation, begin/try and transactions in SQL procedures
* implement proper error handling in Vue pages (showing error messages)
* add filtering (search) and sorting on pages with Professors and Students
* test with the bigger number of users/students